<?php
/**
 * This file is for displaying comments and the comment form
 *
 * @package WordPress
 * @subpackage SGD_Bootstrap_5_Gulp_4_Sass
 * @since SGD Bootstrap 5 Gulp 4 Sass 1.0.0
 */

// if the current post is password protected
if( post_password_required() ) {
	return;
}

$comment_total = get_comments_number();
?>

<style>
.comments-container {
	margin: 0;
	width: 100%;
}

.comments-container > ol,
.comments-container > ul {
	padding-left: 0;
}

.comments-container h1 {
	font-size: 36px;
	color: #283035;
	font-weight: 400;
}

.comments-container h1 a {
	font-size: 18px;
	font-weight: 700;
}

.comments-list {
	margin-top: 30px;
	position: relative;
}

/**
 * Lineas / Detalles
 -----------------------*/
.comments-list:before {
	content: '';
	width: 2px;
	height: 100%;
	background: #c7cacb;
	position: absolute;
	left: 32px;
	top: 0;
}

.comments-list:after {
	content: '';
	position: absolute;
	background: #c7cacb;
	bottom: 0;
	left: 27px;
	width: 7px;
	height: 7px;
	border: 3px solid #dee1e3;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	border-radius: 50%;
}

.reply-list:before, .reply-list:after {display: none;}
.reply-list li:before {
	content: '';
	width: 60px;
	height: 2px;
	background: #c7cacb;
	position: absolute;
	top: 25px;
	left: -55px;
}


.comments-list li {
	margin-bottom: 15px;
	display: block;
	position: relative;
}

.comments-list li:after {
	content: '';
	display: block;
	clear: both;
	height: 0;
	width: 0;
}

.reply-list {
	padding-left: 88px;
	clear: both;
	margin-top: 15px;
	display: inline-block;
}
/**
 * Avatar
 ---------------------------*/
.comments-list .comment-avatar {
	width: 65px;
	height: 65px;
	position: relative;
	float: left;
	border: 3px solid #FFF;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	box-shadow: 0 1px 2px rgba(0,0,0,0.2);
	overflow: hidden;
}

.comments-list .comment-avatar img {
	width: 100%;
	height: 100%;
}

.reply-list .comment-avatar {
	width: 50px;
	height: 50px;
}

.comment-main-level:after {
	content: '';
	width: 0;
	height: 0;
	display: block;
	clear: both;
}
/**
 * Caja del Comentario
 ---------------------------*/
.comments-list .comment-box {
	width: 100%;
	float: right;
	position: relative;
	padding-right: 77px;
	margin-right: -77px;
}

.comments-list .comment-box:before, .comments-list .comment-box:after {
	content: '';
	height: 0;
	width: 0;
	position: absolute;
	display: block;
	border-width: 10px 12px 10px 0;
	border-style: solid;
	border-color: transparent #FCFCFC;
	top: 8px;
	left: -11px;
}

.comments-list .comment-box:before {
	border-width: 11px 13px 11px 0;
	border-color: transparent rgba(0,0,0,0.05);
	left: -12px;
}

.reply-list .comment-box {
	width: 100%;
	margin-right: -62px;
}
.comment-box .comment-head {
	background: #FCFCFC;
	padding: 10px 12px;
	border-bottom: 1px solid #E5E5E5;
	overflow: hidden;
	-webkit-border-radius: 4px 4px 0 0;
	-moz-border-radius: 4px 4px 0 0;
	border-radius: 4px 4px 0 0;
}

.comment-box .comment-head i {
	float: right;
	margin-left: 14px;
	position: relative;
	top: 2px;
	color: #A6A6A6;
	cursor: pointer;
	-webkit-transition: color 0.3s ease;
	-o-transition: color 0.3s ease;
	transition: color 0.3s ease;
	font-size: 1.5em;
}

.comment-box .comment-head i:hover {
	color: #03658c;
}

.comment-box .comment-name {
	color: #283035;
	font-size: 14px;
	font-weight: 700;
	float: left;
	margin: 0 10px 0;
	padding-top: 2px;
}

.comment-box .comment-name a {
	color: #283035;
}

.comment-box .comment-head span {
	color: #999;
	font-size: 13px;
	position: relative;
	padding-left: .5rem;
}
.comment-box .comment-content {
	background: #fff;
	padding: 12px;
	font-size: 15px;
	color: #595959;
	-webkit-border-radius: 0 0 4px 4px;
	-moz-border-radius: 0 0 4px 4px;
	border-radius: 0 0 4px 4px;
}

.comment-box .comment-content > p {
	margin-bottom: 0;
}

.comment-box .comment-name.by-author, .comment-box .comment-name.by-author a {color: #03658c;}
.comment-box .comment-name.by-author:after {
	content: 'author';
	background: #03658c;
	color: #FFF;
	font-size: 12px;
	padding: 3px 5px;
	font-weight: 700;
	margin-left: 10px;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
}
.comment-form-comment textarea {
	width: 100%;
}
</style>

<div id="comments" class="comments-container <?php echo get_option( 'show_avatars' ) ? 'show-avatars' : ''; ?>">

	<?php if ( have_comments() ) : ?>
	<h2 class="comments-title mt-4">
		<i class="fa fa-comments fa-fw"></i>
		<?php if ( $comment_total === 1 ) : ?>
			<?php esc_html_e( '1 comment', 'sgd-bs5-gulp4-sass' ); ?>
		<?php else : ?>
			<?php
			printf(
				/* translators: %s: Comment count number. */
				esc_html( _nx( '%s comment', '%s comments', $comment_total, 'Comments title', 'sgd-bs5-gulp4-sass' ) ),
				esc_html( number_format_i18n( $comment_total ) )
			);
			?>
		<?php endif; ?>
	</h2><!-- .comments-title -->

	<ol class="comments-list">
	<?php
	class Custom_Walker_Comment extends Walker_Comment {
		public function start_lvl( &$output, $depth = 0, $args = array() ) {
			$GLOBALS['comment_depth'] = $depth + 1;

			switch ( $args['style'] ) {
				case 'div':
					break;
				case 'ol':
					$output .= '<ol class="comments-list reply-list reply-list-'.$depth.'">' . "\n";
					break;
				case 'ul':
				default:
					$output .= '<ul class="comments-list reply-list reply-list-'.$depth.'">' . "\n";
					break;
			}
		}
		protected function comment( $comment, $depth, $args ) {
			if ( 'div' === $args['style'] ) {
				$tag       = 'div';
				$add_below = 'comment';
			} else {
				$tag       = 'li';
				$add_below = 'div-comment';
			}

			$commenter          = wp_get_current_commenter();
			$show_pending_links = isset( $commenter['comment_author'] ) && $commenter['comment_author'];

			if ( $commenter['comment_author_email'] ) {
				$moderation_note = __( 'Your comment is awaiting moderation.' );
			} else {
				$moderation_note = __( 'Your comment is awaiting moderation. This is a preview; your comment will be visible after it has been approved.' );
			}
			?>
			<<?php echo $tag; ?> <?php comment_class( $this->has_children ? 'parent' : '', $comment ); ?> id="comment-<?php comment_ID(); ?>">
			<?php if ( 'div' !== $args['style'] ) : ?>
			<div id="div-comment-<?php comment_ID(); ?>" class="comment-body comment-main-level">
			<?php endif; ?>
			
			<?php
			if ( 0 != $args['avatar_size'] ) { ?>
			<div class="comment-avatar">
			<?php echo get_avatar( $comment, $args['avatar_size'] ); ?>
			</div>
			<?php } ?>
			<div class="comment-box">
				<div class="comment-head">
				<?php
				$comment_author = get_comment_author_link( $comment );

				if ( '0' == $comment->comment_approved && ! $show_pending_links ) {
					$comment_author = get_comment_author( $comment );
				}

				// echo '<h6 class="comment-name by-author">';
				echo $comment_author;
				// echo '</h6>';

				// printf(
				// 	/* translators: %s: Comment author link. */
				// 	__( '%s <span class="says">says:</span>' ),
				// 	sprintf( '<cite class="fn">%s</cite>', $comment_author )
				// );
				?>
				<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php echo $moderation_note; ?></em>
				<?php endif; ?>
				<?php
				printf(
					'<span>%s</span>',
					sprintf(
						/* translators: 1: Comment date, 2: Comment time. */
						__( '%1$s at %2$s' ),
						get_comment_date( '', $comment ),
						get_comment_time()
					)
				);
				?>
				
				<?php // edit_comment_link( __( '<i class="fa fa-edit">' ), '', '' ); ?>
				<a href="<?php echo get_edit_comment_link(); ?>"><i class="fa fa-edit fa-fw"></i></a>

				<?php /*<a href=""><i class="fa fa-mail-reply"></i></a>
				<a href=""><i class="fas fa-heart"></i></a>*/ ?>
			</div>
			<div class="comment-content">
			<?php
			comment_text(
				$comment,
				array_merge(
					$args,
					array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
					)
				)
			);
			?>

			<?php
			comment_reply_link(
				array_merge(
					$args,
					array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
						'before'    => '<div class="reply mt-1">',
						'after'     => '</div>',
						'reply_text'=> __('<i class="fa fa-mail-reply fa-fw"></i> Reply', 'textdomain')
					)
				)
			);
			?>
			</div>

			<?php if ( 'div' !== $args['style'] ) : ?>
			</div>
			<?php endif; ?>
			<?php
		}
	}
	wp_list_comments(
		array(
			'max_depth'   => '2',
			'avatar_size' => 65,
			'style'       => 'ol',
			'short_ping'  => true,
			'walker'      => new Custom_Walker_Comment()
		)
	);
	?>
	</ol>

	<?php
	the_comments_pagination(
		array(
			'before_page_number' => esc_html__('Page', 'sgd-bs5-gulp4-sass').' ',
			'mid_size'           => 0,
			'prev_text'          => (is_rtl() ? '<i class="fa fa-angle-right"></i>' : '<i class="fa fa-angle-left"></i>'),
			'next_text'          => (is_rtl() ? '<i class="fa fa-angle-left"></i>' : '<i class="fa fa-angle-right"></i>')
		)
	);
	?>

	<?php if ( ! comments_open() ) : ?>
		<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'sgd-bs5-gulp4-sass' ); ?></p>
	<?php endif; ?>
<?php endif; ?>

<?php
comment_form(
	array(
		'title_reply'        => esc_html__( 'Leave a Comment', 'sgd-bs5-gulp4-sass' ),
		'title_reply_before' => '<h2 id="reply-title" class="comment-reply-title">',
		'title_reply_after'  => '</h2>',
	)
);
?>

</div><!-- #comments -->
