<?php
/**
 * Basic WooCommerce support
 * For an alternative integration method see WC docs
 * http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
 */
?>

<?php get_header(); ?>

<?php
if(is_shop()
|| is_product_category()
|| is_product_tag()
|| is_cart()
|| is_checkout()
|| is_account_page()
|| is_wc_endpoint_url()) {
	$sidebar = false;
}
elseif(is_product()) {
	$sidebar = false;
}
else {
	$sidebar = false;
}
//https://docs.woocommerce.com/document/conditional-tags/
/*
is_woocommerce()
Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and thus are not included).
Main shop page
is_shop()
Returns true when on the product archive page (shop).
Product category page
is_product_category()
Returns true when viewing a product category archive.
is_product_category( 'shirts' )
When the product category page for the ‘shirts’ category is being displayed.
is_product_category( array( 'shirts', 'games' ) )
When the product category page for the ‘shirts’ or ‘games’ category is being displayed.
Product tag page
is_product_tag()
Returns true when viewing a product tag archive
is_product_tag( 'shirts' )
When the product tag page for the ‘shirts’ tag is being displayed.
is_product_tag( array( 'shirts', 'games' ) )
When the product tag page for the ‘shirts’ or ‘games’ tags is being displayed.
Single product page
is_product()
Returns true on a single product page. Wrapper for is_singular.
Cart page
is_cart()
Returns true on the cart page.
Checkout page
is_checkout()
Returns true on the checkout page.
Customer account pages
is_account_page()
Returns true on the customer’s account pages.
Endpoint
is_wc_endpoint_url()
Returns true when viewing a WooCommerce endpoint
is_wc_endpoint_url( 'order-pay' )
When the endpoint page for order pay is being displayed.
is_wc_endpoint_url( 'order-received' )
When the endpoint page for order received is being displayed.
is_wc_endpoint_url( 'view-order' )
When the endpoint page for view order is being displayed.
is_wc_endpoint_url( 'edit-account' )
When the endpoint page for edit account is being displayed.
is_wc_endpoint_url( 'edit-address' )
When the endpoint page for edit address is being displayed.
is_wc_endpoint_url( 'lost-password' )
When the endpoint page for lost password is being displayed.
is_wc_endpoint_url( 'customer-logout' )
When the endpoint page for customer logout  is being displayed.
is_wc_endpoint_url( 'add-payment-method' )
When the endpoint page for add payment method is being displayed.
Ajax request
is_ajax()
*/
?>

<section class="section-1 section-hero section-hero-shop-category">
	<div class="section-hero-img">
		<?php
		// if(is_shop()) {
		// 	//echo '<h1>Shop Page</h1>';
		// }
		// elseif(is_product_category()) {
		// 	global $wp_query;
		// 	$cat = $wp_query->get_queried_object();
		// 	//$thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
		// 	$category_hero = get_field('hero_image',$cat);
		// 	//$image = wp_get_attachment_url( $thumbnail_id );
		// 	//if ( $image ) {
		// 	if ($category_hero) {
		// 		echo '<img src="'.$category_hero['sizes']['featured-large'].'" alt="'.$cat->name.'" style="width: 100%;height: auto;" />';
		// 		//echo '<pre>';
		// 		//print_r($category_hero);
		// 		//echo '</pre>';
		// 	}
		// }
		?>
    </div>
</section>

<section id="section-2" <?php post_class('section section-woocommerce'); ?>>
    <div class="container">
        <?php woocommerce_content(); ?>
    </div>
</section>

<?php get_footer(); ?>