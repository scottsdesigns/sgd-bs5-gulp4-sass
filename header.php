<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage SGD_Bootstrap_5_Gulp_4_Sass
 * @since SGD Bootstrap 5 Gulp 4 Sass 1.0.0
 */

if(function_exists('the_custom_logo')) {
    if(has_custom_logo()) {
        the_custom_logo();
    }
    else {
        // echo "No logo";
        // $brand_image = get_template_directory_uri().'/assets/img/trx400ex.com-logo-w-skull-black-w-shadow_238x46';
        $brand_image = get_template_directory_uri().'/assets/img/trx400ex.com-logo-white-w-shadow_451x111.png';
    }
}
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php //bloginfo('name'); ?><?php is_front_page() ? bloginfo('name') : wp_title(' | '); ?></title>
<!-- begin favicons -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/honda-trx400ex-favicon.ico"><!-- 32×32 -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/honda-trx400ex-favicon.svg" type="image/svg+xml">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/honda-trx400ex-favicon_180.png">
<?php /* ----- old way -----
<!-- default -->
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon.ico" />
<!-- ios -->
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-152.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-144.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-120.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-114.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-76.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-72.png">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-57.png">
<!-- android -->
<link rel="shortcut icon" sizes="196x196" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-196.png">
<!-- ie -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/img/favicons/favicon-144.png">
<!-- end favicons -->*/ ?>
<?php /*
<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@ScottGeithman">
<meta name="twitter:title" content="<?php bloginfo('name'); ?>">
<meta name="twitter:description" content="All things 1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, and 2008-2014 TRX400X">
<meta name="twitter:creator" content="@ScottGeithman">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/assets/img/social/11_gallery_atv_og_card_800x450.jpg">
<meta name="twitter:domain" content="<?php echo esc_url(home_url()); ?>">*/ ?>

<!-- Open Graph - for facebook and twitter shares
================================================== -->   
<meta property="og:title" content="<?php bloginfo('name'); ?>" />
<meta property="og:description" content="All things 1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, and 2008-2014 TRX400X" />
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/trx400ex.com-card_800x450.jpg" />
<meta property="og:url" content="<?php echo esc_url(home_url()); ?>" />

<!-- ItemProp - for google shares
================================================== -->  
<meta property="itemprop:name" content="<?php bloginfo('name'); ?>">
<meta property="itemprop:description" content="All things 1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, and 2008-2014 TRX400X">
<meta property="itemprop:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/trx400ex.com-card_800x450.jpg">

<?php wp_head(); ?>

<style>
body {
    margin-top: 72px;
}
body.admin-bar #nav-main {
    margin-top: 32px;
}
@media screen and (max-width: 782px) {
    body.admin-bar #nav-main {
        margin-top: 46px;
    }
}
@media screen and (max-width: 600px) {
    body.admin-bar #nav-main {
        margin-top: 0;
    }
}
</style>

<?php /* if($_SERVER['SERVER_NAME'] != '11galleryatv.wp') { ?>
<!-- Local Google AdSense code for 11 Gallery ATV http://11galleryatv.wp -->
<script data-ad-client="ca-pub-3226020998547558" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- End of Google AdSense Code -->
<?php } else { ?>
<!-- Local Google AdSense code for 11 Gallery ATV http://11galleryatv.wp -->
<!-- End of Google AdSense Code -->
<?php } */ ?>
</head>

<body <?php body_class(); ?>  data-bs-spy="scroll" data-bs-target="#nav-main" data-bs-offset="40" class="bs5-scrollspy" tabindex="0">
  
    <!-- Navbar Top Fixed
    ==================================================-->
    <nav id="nav-main" class="navbar navbar-expand-md fixed-top bg-primary navbar-dark nav-up">

        <div class="container">

            <!-- Navbar Brand -->
            <a class="navbar-brand xs d-md-none" href="<?php echo esc_url(home_url()); ?>" title="1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, 2008-2014 TRX400X"><img src="<?php echo $brand_image; ?>" title="1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, 2008-2014 TRX400X" alt="logo" class="logo xs" style="width: auto;height: 46px;"></a>
            <a class="navbar-brand md d-none d-md-block" href="<?php echo esc_url(home_url()); ?>"><img src="<?php echo $brand_image; ?>" title="1999-2004 Honda TRX400EX, 2005-2007 Honda TRX400EX, 2008-2014 TRX400X" alt="logo" class="logo md" style="width: auto;height: 46px;"></a>

            <!-- Offcanvas Navbar -->
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvas-navbar">
                <div class="offcanvas-header bg-dark">
                <span class="h5 mb-0">Menu</span>
                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body bg-primary">
                <!-- Bootstrap 5 Nav Walker Main Menu -->
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'in_header',
                    'menu'            => 'Main Menu',
                    'container'       => false,
                    'menu_class'      => '',
                    'fallback_cb'     => '__return_false',
                    'items_wrap'      => '<ul id="bootscore-navbar" class="navbar-nav ms-auto %2$s">%3$s</ul>',
                    'depth'           => 2,
                    'walker'          => new bootstrap_5_1_3_wp_nav_menu_walker()
                ));
                ?>
                <!-- Bootstrap 5 Nav Walker Main Menu End -->
                </div>
            </div>


            <div class="header-actions d-flex align-items-center">

                <!-- Top Nav Widget -->
                <div class="top-nav-widget">
                <?php if (is_active_sidebar('top-nav')) : ?>
                    <div>
                    <?php dynamic_sidebar('top-nav'); ?>
                    </div>
                <?php endif; ?>
                </div>
<?php /*
                <!-- Searchform Large -->
                <div class="d-none d-lg-block ms-1 ms-md-2 top-nav-search-lg">
                <?php if (is_active_sidebar('top-nav-search')) : ?>
                    <div>
                    <?php dynamic_sidebar('top-nav-search'); ?>
                    </div>
                <?php endif; ?>
                </div>
*/ ?>
<?php /*
                <!-- Search Toggler Mobile -->
                <button class="btn btn-outline-secondary d-lg-none ms-1 ms-md-2 top-nav-search-md" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-search" aria-expanded="false" aria-controls="collapse-search">
                    <i class="fas fa-search"></i>
                </button>
*/ ?>
                <!-- Navbar Toggler -->
                <button class="btn btn-outline-secondary text-white d-md-none ms-1 ms-md-2" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas-navbar" aria-controls="offcanvas-navbar">
                    <i class="fas fa-bars"></i>
                </button>

            </div><!-- .header-actions -->

        </div><!-- .container -->

    </nav><!-- .navbar -->
<?php /*
    <!-- Top Nav Search Mobile Collapse -->
    <div class="collapse container d-lg-none" id="collapse-search">
        <?php if (is_active_sidebar('top-nav-search')) : ?>
        <div class="mb-2">
        <?php dynamic_sidebar('top-nav-search'); ?>
        </div>
        <?php endif; ?>
    </div>
*/ ?>    
<main class="wrapper">
    
    <?php get_template_part('template-parts/sections','hero'); ?>
