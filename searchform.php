<?php /*<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<input type="search" class="search-field" placeholder="Search …" value="<?php echo get_search_query() ?>" name="s" title="Search for:" />
	</label>
	<input type="submit" class="search-submit btn-primary" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
</form>*/ ?>

<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
		<input name="s" class="form-control border-end-0 border" type="search" value="<?php echo get_search_query() ?>">
		<span class="input-group-append">
			<button class="btn btn-primary fw-bold border-start-0 border-bottom-0 border ms-n5" type="button">
				<i class="fa fa-search"></i> Search
			</button>
		</span>
	</div>
</form>