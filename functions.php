<?php
//----- base stuff -----
require get_parent_theme_file_path('/includes/enqueue-scripts.php');
require get_parent_theme_file_path('/includes/navigation.php');
require get_parent_theme_file_path('/includes/widget-areas.php');
require get_parent_theme_file_path('/includes/wp-bootstrap5.1.3-navwalker.php');
require get_parent_theme_file_path('/includes/theme.php');

//----- shortcodes -----
require get_parent_theme_file_path('/shortcodes/get_posts_by_category.php');
require get_parent_theme_file_path('/shortcodes/get_portfolio_items.php');
require get_parent_theme_file_path('/shortcodes/get_blog_items.php');
require get_parent_theme_file_path('/shortcodes/get_custom_query.php');

//----- custom stuff -----
require get_parent_theme_file_path('/custom/login.php');

//----- plugin stuff -----
require get_parent_theme_file_path('/plugins/advanced-custom-fields.php');
require get_parent_theme_file_path('/plugins/woocommerce.php');

//----- admin stuff -----
require get_parent_theme_file_path('/admin/enqueue-admin-scripts.php');
require get_parent_theme_file_path('/admin/admin-bar.php');
require get_parent_theme_file_path('/admin/customizer.php');
require get_parent_theme_file_path('/admin/custom-columns.php');

//----- blocks -----
require get_parent_theme_file_path('/blocks/_add-block-categories.php');
require get_parent_theme_file_path('/blocks/block_image-gallery.php');