<?php
function add_woocommerce_support() {
	add_theme_support('woocommerce');
	add_theme_support('wc-product-gallery-zoom');
	add_theme_support('wc-product-gallery-lightbox');
	add_theme_support('wc-product-gallery-slider');
}
add_action('after_setup_theme', 'add_woocommerce_support');

//WooCommerce Navigation
register_nav_menu('woocommerce_menu','WooCommerce Menu');

//----- Remove WooCommerce Styling/ -----
// Remove each style one by one
// add_filter('woocommerce_enqueue_styles', 'filter_woocommerce_enqueue_styles');
// function filter_woocommerce_enqueue_styles( $enqueue_styles) {
// 	unset($enqueue_styles['woocommerce-general']);	    // Remove the gloss
// 	unset($enqueue_styles['woocommerce-layout']);		// Remove the layout
// 	unset($enqueue_styles['woocommerce-smallscreen']);	// Remove the smallscreen optimization
// 	return $enqueue_styles;
// }
// Or just remove them all in one line
//add_filter( 'woocommerce_enqueue_styles', '__return_false' );
//----- /Remove WooCommerce Styling -----

//WooCommerce Widgets
// Sidebar for woocommerce pages
function woocommerce_sidebar_widget() {
	register_sidebar(array(
		'name' => __('Shop Sidebar', 'ecmaster'),
		'id' => 'sidebar-shop',
		'description' => __('Sidebar for Shop Page', 'ecmaster'),
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h4>',
		'after_title' => '</h4>'
	));
}
add_action('widgets_init', 'woocommerce_sidebar_widget');

function woocommerce_cart_widget() {
	register_sidebar(array(
		'id' => 'cart-page-message',
		'name' => __('Cart Page Message', 'ecmaster'),
		'description' => __('This Widget is displayed on the WooCommerce Cart page', 'ecmaster'),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
}
add_action('widgets_init', 'woocommerce_cart_widget');

// WooCommerce Cart
function cart_widget($content) {
	dynamic_sidebar('cart-page-message');
	return $content;
}
add_filter('woocommerce_cart_collaterals', 'cart_widget');

// WooCommerce Thankyou
function woocommerce_thank_you_widget() {
	register_sidebar(array(
		'id' => 'thank-you-page-message',
		'name' => __('Thank you Page Message', 'ecmaster'),
		'description' => __('This Widget is displayed on the WooCommerce Thank You page', 'ecmaster'),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h6>',
		'after_title' => '</h6>',
	));
}
add_action('widgets_init', 'woocommerce_thank_you_widget');

function thanks_widget() {
	dynamic_sidebar('thank-you-page-message');
}
add_action('woocommerce_thankyou', 'thanks_widget', 1);

// Woocommerce theme support in /library/theme-support.php

// Change number or products per row to 3
// add_filter('loop_shop_columns', 'loop_columns');
// if(!function_exists('loop_columns')) {
// 	function loop_columns() {
// 		return 3; // 3 products per row
// 	}
// }

// add_filter('widget_display_callback', 'rename_widget', 10, 3);
// function rename_widget($settings, $widget, $args) {
// 	//Custom_WooCommerce_Widget_Cart
// 	if(get_class($widget) == 'WC_Widget_Product_Categories') {
// 		$settings['title'] = 'Git-R-Done';
// 	}
// 	return $settings;
// }

// Mini Cart
/*add_filter('woocommerce_add_to_cart_fragments', 'minicart_button');
function minicart_button($fragments) {
   global $woocommerce;
   ob_start();
   ?>
	 <button class="cart-customlocation"><span class="fa fa-shopping-cart"></span>&nbsp; <?php echo sprintf (_n('%d item', '%d items', $woocommerce->cart->get_cart_contents_count()), $woocommerce->cart->get_cart_contents_count()); ?></button>
   <?php
   $fragments['.cart-customlocation'] = ob_get_clean();
   return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'minicart_dropdown');
function minicart_dropdown($fragments) {
		ob_start(); ?>
		<div class="mini-cart-container hide-cart">
		<div class="close"><span class="exit thin"></span></div>
		<?php get_template_part('template-parts/mini-cart'); ?>
	 </div>
	<?php
   $fragments['.mini-cart-container'] = ob_get_clean();
   return $fragments;
}

add_filter('get_product_search_form' , 'custom_product_searchform');
function custom_product_searchform($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . esc_url(home_url('/')) . '">
				<div class="input-group">
					<label class="screen-reader-text" for="s">' . __('Search for:', 'ecmaster') . '</label>
					<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __('Search products', 'ecmaster') . '" class="input-group-field" />
					<div class="input-group-button">
						<button type="submit" id="searchsubmit" class="button" />'. esc_attr__('Search', 'ecmaster') .'</button>
					</div>
					<input type="hidden" name="post_type" value="product" />
				</div>
			</form>';
	return $form;
}*/

/**
 * Display category image on category archive
 */
/*add_action('woocommerce_archive_description', 'woocommerce_category_image', 2);
function woocommerce_category_image() {
    if(is_product_category()){
	    global $wp_query;
	    $cat = $wp_query->get_queried_object();
		//$thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
		$category_hero = get_field('hero_image',$cat);
	    //$image = wp_get_attachment_url($thumbnail_id);
		//if($image) {
		if($category_hero) {
			echo '<img src="'.$category_hero['sizes']['featured-large'].'" alt="'.$cat->name.'" />';
			//echo '<pre>';
			//print_r($category_hero);
			//echo '</pre>';
		}
	}
}*/

// This will take care of the Buy Product button below the external product on the Shop page.
add_filter( 'woocommerce_loop_add_to_cart_link', 'ts_external_add_product_link' , 10, 2 );

// Remove the default WooCommerce external product Buy Product button on the individual Product page.
remove_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );

// Add the open in a new browser tab WooCommerce external product Buy Product button.
add_action( 'woocommerce_external_add_to_cart', 'ts_external_add_to_cart', 30 );


function ts_external_add_product_link($link) {
	global $product;
	if($product->is_type('external')) {
		$link = sprintf('<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" target="_blank">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			esc_attr( $product->id ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $class ) ? $class : 'button product_type_external' ),
			esc_html( $product->add_to_cart_text() )
		);
	}
	return $link;
}

function ts_external_add_to_cart() {
	global $product;
	if (!$product->add_to_cart_url()) {
		return;
	}
	$product_url = $product->add_to_cart_url();
	$button_text = $product->single_add_to_cart_text();
	/**
	* The code below outputs the edited button with target="_blank" added to the html markup.
	*/
	do_action( 'woocommerce_before_add_to_cart_button' ); ?>
	<p class="cart">
	<a href="<?php echo esc_url($product_url); ?>" rel="nofollow" class="single_add_to_cart_button button alt" target="_blank">
	<?php echo esc_html($button_text); ?></a>
	</p>
	<?php do_action('woocommerce_after_add_to_cart_button');
}

/**
 * Changes the external product button's add to cart text
 *
 * @param string $button_text the button's text
 * @param \WC_Product $product
 * @return string - updated button text
 */
function sv_wc_external_product_button($button_text, $product) {
	$product_type = $product->get_type();
	switch($product_type) {
		case 'external':
			// change text based on category (amazon or ebay)
			$primary_cat_name = '';
			$primary_cat_id = get_post_meta($product->get_id(),'_yoast_wpseo_primary_product_cat',true);
			if($primary_cat_id) {
				$product_cat = get_term($primary_cat_id, 'product_cat');
				if(isset($product_cat->name)) {
					$primary_cat_name = $product_cat->name;
				}
			}
			switch($primary_cat_name) {
				case 'Amazon':
					return 'Buy on Amazon ◳';
				break;
				case 'eBay':
					return 'Buy on eBay ◳';
				break;
				default:
					return $product->button_text ? $product->button_text : 'Buy Now';
			}
			
			// $terms = get_the_terms( $product->ID, 'product_cat' );
			// 	foreach ($terms as $term) {
			// 		$product_cat = $term->name;
			// 		break;
			// 	}
			// switch( $product_cat ) {
			// 	case 'Amazon':
			// 		return 'Buy on Amazon ◳';
			// 	break;
			// 	case 'eBay':
			// 		return 'Buy on eBay ◳';
			// 	break;
			// 	default:
			// 		return $product->button_text ? $product->button_text : 'Buy Now';
			// }


			// return $product->button_text ? $product->button_text : var_dump($product);
			// break;
		// case 'grouped':
		// 		return $product->button_text ? $product->button_text : 'Buy Now';
		// 	break;
		// case 'simple':
		// 		return $product->button_text ? $product->button_text : 'Buy Now';
		// 	break;
		// case 'variable':
		// 		return $product->button_text ? $product->button_text : 'Buy Now';
		// 	break;
		default:
			return $product->button_text ? $product->button_text : 'Buy Product';
	}
    return $button_text;
}
add_filter('woocommerce_product_single_add_to_cart_text', 'sv_wc_external_product_button', 10, 2); // single page
add_filter('woocommerce_product_add_to_cart_text', 'sv_wc_external_product_button', 10, 2); // shop page