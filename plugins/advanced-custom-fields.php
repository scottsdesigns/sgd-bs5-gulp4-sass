<?php
// Theme Option Page
/*if(function_exists('acf_add_options_page')) {
	acf_add_options_page();
}
if(function_exists('acf_set_options_page_title')) {
    acf_set_options_page_title(__('Global Settings'));
}
if(function_exists('acf_add_options_sub_page')) {
    acf_add_options_sub_page('Site Settings');
    acf_add_options_sub_page('Site Keys');
}*/
// Default image to image field in new post or page
add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
function add_default_value_to_image_field($field) {
    $args = array(
        'label' => 'Default Image',
        'instructions' => 'Appears when creating a new post',
        'type' => 'image',
        'name' => 'default_value'
    );
    acf_render_field_setting($field, $args);
}
add_action('admin_enqueue_scripts', 'enqueue_uploader_for_image_default');
function enqueue_uploader_for_image_default() {
    $screen = get_current_screen();
    if ($screen && $screen->id && ($screen->id == 'acf-field-group')) {
        acf_enqueue_uploader();
    }
}
add_filter('acf/load_value/type=image', 'reset_default_image', 10, 3);
function reset_default_image($value, $post_id, $field) {
    if (!$value) {
        $value = $field['default_value'];
    }
    return $value;
}
// html enitities before saving to database for markdown field
add_filter('acf/update_value/key=field_5a93080349e5a', 'before_acf_save', 10, 3);//5a99f11152e00 live site field key
function before_acf_save($value, $field, $post_id) {
    $value = htmlentities($value);
    return $value;
}
// html decode enitities when loaded from database for markdown field
add_filter('acf/load_value/key=field_5a93080349e5a', 'after_acf_load', 10, 3);//5a99f11152e00 live site field key
function after_acf_load($value, $post_id, $field) {
    $value = html_entity_decode($value);
    return $value;
}
