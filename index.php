<?php get_header(); ?>
    
<style>
article {
    background-color: #fff;
    border: 1px solid #ccc;
}
</style>
    <section id="post-<?php the_ID(); ?>" <?php post_class('section section-page'); ?>>
        <div class="container">
            <div class="row">
				<?php if(have_posts()) : while (have_posts()) : the_post(); ?>
                <?php
                if(has_post_thumbnail()) {
                    $thumb = get_the_post_thumbnail_url(get_the_ID(), 'large');
                }
                else {
                    $thumb = get_template_directory_uri().'/assets/img/default/no-image-dk-text_600x450.jpg';
                }
                ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 mt-3 mb-3">
                    <div class="card">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <img src="<?php echo $thumb; ?>" class="card-img-top" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>"/>
                        </a>
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="card-body text-center p-1">
                                <h5 class="text-center px-2 my-3"><?php echo get_the_title(); ?></h5>
                            </div>
                        </a>
                        <div class="p-1">
                            <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary w-100">View Post</a>
                        </div>
                    </div>
                </div>
                
                <?php /*<h1><?php the_title(); ?></h1>
                <?php //the_content(); ?>
                <?php
                $my_excerpt = get_the_excerpt();
                if($my_excerpt != '') {
                    // Some string manipulation performed
                }
                echo $my_excerpt; // Outputs the processed value to the page
                ?>*/ ?>
                <?php endwhile; else: ?>
                <section class="post-none" style="margin-left: 15px;padding: 40px 0 60px 0;">
                	<h3 class="media-heading"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h3>
                </section>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>