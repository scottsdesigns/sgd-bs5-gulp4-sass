<?php
//----- "Template" column to admin page list -----
function page_column_views($columns) {
    //remove comments column
    unset($columns['comments']);
    $arr = array_merge($columns,array('page-layout'=>__('Template')));
    //re-order the columns to preferred order
    $new_keys = array('cb','title','page-layout','author',/*'comments',*/'date'); //uncomment comments if you remove the unset above
    $new_data = array();
    foreach ($new_keys as $key) {
        $new_data[$key] = $arr[$key];
    }
    return $new_data;
}
add_filter('manage_pages_columns', 'page_column_views');
function page_custom_column_views($column_name, $id) {
   if ($column_name === 'page-layout') {
       $set_template = get_post_meta(get_the_ID(), '_wp_page_template', true);
       if ($set_template == 'default') {
           echo 'Default';
       }
       $templates = get_page_templates();
       ksort($templates);
       foreach (array_keys($templates) as $template) {
           if($set_template == $templates[$template]) {
               echo $template;
           }
       }
   }
}
add_action('manage_pages_custom_column', 'page_custom_column_views', 10, 2);