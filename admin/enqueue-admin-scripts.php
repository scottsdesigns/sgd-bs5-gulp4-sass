<?php
// add style sheets to admin
function namespaced_admin_styles_function() {
	echo '<link href="'.get_template_directory_uri().'/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css"  rel="stylesheet">';
}
add_action('admin_head', 'namespaced_admin_styles_function');