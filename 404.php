<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

<head>
    <meta charset="utf-8" />
    <title>404 - Error</title>
    <meta name="keywords" content="" />
    <meta name="description" content="404 - Page Template" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!-- Libs CSS -->
    <link type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/404/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Template CSS -->
    <link type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/404/css/style.css" rel="stylesheet" />
    <!-- Responsive CSS -->
    <link type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/404/css/respons.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900" rel="stylesheet" type="text/css">

</head>
<body>

    <!-- Load page -->
    <div class="animationload">
        <div class="loader">
        </div>
    </div>
    <!-- End load page -->

    <!-- Content Wrapper -->
    <div id="wrapper">
        <div class="container">
            <!-- Switcher -->
            <div class="switcher">
                <input id="sw" type="checkbox" class="switcher-value">
                <label for="sw" class="sw_btn"></label>
                <div class="bg"></div>
                <div class="text">Turn <span class="text-l">off</span><span class="text-d">on</span><br />the light</div>
            </div>
            <!-- End Switcher -->

            <!-- Dark version -->
            <div id="dark" class="row text-center">
                <div class="info">
                    <img src="<?php echo get_template_directory_uri(); ?>/404/img/404-dark.png" alt="404 error" />
                </div>
            </div>
            <!-- End Dark version -->

            <!-- Light version -->
            <div id="light" class="row text-center">
                <!-- Info -->
                <div class="info">
                    <img src="<?php echo get_template_directory_uri(); ?>/404/img/404-light.gif" alt="404 error" />
                    <!-- end Rabbit -->
                    <p>The page you are looking for was moved, removed,<br />
                        renamed or might never existed.</p>
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-brown">Home Page</a>
                </div>
                <!-- end Info -->
            </div>
            <!-- End Light version -->

        </div>
        <!-- end container -->
    </div>
    <!-- end Content Wrapper -->


    <!-- Scripts -->
    <script src="<?php echo get_template_directory_uri(); ?>/404/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/404/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/404/js/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/404/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/404/js/scripts.js" type="text/javascript"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>