<style>
.above-footer a {
    color: #fc0;
}
.above-footer a:hover {
    color: #970;
}
.footer a {
    color: #9ad260;
}
.footer a:hover {
    color: #7ba84c;
}
@media (max-width: 575px) {
    .section-widget {
        padding: 0;
    }
}
</style>  
   <?php if(is_active_sidebar('above-footer-section-1')) { ?>
   <section class="section section-widget">
        <?php dynamic_sidebar('above-footer-section-1'); ?>
    </section>
    <?php } ?>
    <?php if(is_active_sidebar('above-footer-section-2')) { ?>
   <section class="section section-widget">
        <div class="container">
        <?php dynamic_sidebar('above-footer-section-2'); ?>
        </div>
    </section>
    <?php } ?>
    
</main>
    
    <section class="section text-white" style="background-color: #4d0000;border-bottom: 1px solid #000000;padding: 40px 0;">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-10">
                    <h4 class="media-heading">
                        11 Gallery ATV - custom YouTube builds. <a href="https://www.youtube.com/11GalleryATV" target="_blank">YouTube.com/11GalleryATV</a> - Latest complete build: Honda 400ex from scratch.
                    </h4>
                </div>
                <div class="col-sm-3 col-md-2">
                    <a href="https://www.youtube.com/11GalleryATV" target="_blank" class="btn btn-light btn-block" style="margin-top: 10px;"><i class="fab fa-youtube mr-1"></i> View Videos</a>
                </div>
            </div>
        </div>
    </section>
    
	<section class="above-footer text-white" style="background-color: #222222;padding: 40px 0;">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-4 col-sm-6 mb-5 mb-lg-0">
				<h4 class="mb-3">Recent Posts</h4>
				
				<?php
                $post_query = new WP_Query(array(
                    'posts_per_page' => 4,
                    'post_type'      => 'post',
                    'post_status'    => 'publish',
					//'cat'            => 12,
                    //'paged'          => $paged,
                    //'offset'         => offset(1, $page),
                    //'paged'          => $page,
                    'orderby'        => 'date',
                    'order'          => 'DESC'
                ));
                //$output = $post_query;
                
                if($post_query->have_posts()) {
                    $post_count = 0;
                    // $output .= '<div class="row">';
                    while($post_query->have_posts()) {
                        $post_query->the_post();
                        if(has_post_thumbnail()) {
                        $thumb = get_the_post_thumbnail_url(null, /*'thumbnail'*/ 'medium');
                        }
                        else {
                            $thumb = get_template_directory_uri().'/assets/img/default/11-gallery-atv-logo-stacked-white-333333-w-shadow_600x450.jpg';
                        }
					?>
                        <div class="card text-white bg-black mb-3">
                            <div class="row g-0">
                                <div class="col-4">
                                    <a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                        <img src="<?php echo $thumb; ?>" class="img-fluid rounded-start" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
                                    </a>
                                </div>
                                <div class="col-8">
                                    <div class="card-body">
                                        <a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                            <h6 class="card-title"><?php echo get_the_title(); ?></h6>
                                        </a>
                                        <p class="recent-post-date"><small><?php echo get_the_date(); ?></small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        $post_count++;
                    }
                    // $output .= '</div>';
                    wp_reset_query();
                    wp_reset_postdata();
                }
				?>
                    <?php
                    if(is_active_sidebar('above-footer-column-1')) {
                        ob_start();
                        dynamic_sidebar('above-footer-column-1');
                        $sidebar1 = ob_get_contents();
                        ob_end_clean();
                        $sidebar_corrected_ul1 = str_replace("<ul>", '<ul class="list-unstyled">', $sidebar1);
                        echo $sidebar_corrected_ul1;
                    }
                    ?>
                </div>
                
                <div class="col-lg-2 col-sm-6 mb-5 mb-lg-0">
                    <?php
                    if(is_active_sidebar('above-footer-column-2')) {
                        ob_start();
                        dynamic_sidebar('above-footer-column-2');
                        $sidebar2 = ob_get_contents();
                        ob_end_clean();
                        $sidebar_corrected_ul2 = str_replace("<ul>", '<ul class="list-unstyled">', $sidebar2);
                        echo $sidebar_corrected_ul2;
                    }
                    ?>
                </div>
                
                <div class="col-lg-2 col-sm-4 mb-5 mb-sm-0">
                    <?php
                    if(is_active_sidebar('above-footer-column-3')) {
                        ob_start();
                        dynamic_sidebar('above-footer-column-3');
                        $sidebar3 = ob_get_contents();
                        ob_end_clean();
                        $sidebar_corrected_ul3 = str_replace("<ul>", '<ul class="list-unstyled">', $sidebar3);
                        echo $sidebar_corrected_ul3;
                    }
                    ?>
            	</div>

                <div class="col-lg-4 col-sm-8 mb-0">
                    <?php
                    if(is_active_sidebar('above-footer-column-4')) {
                        ob_start();
                        dynamic_sidebar('above-footer-column-4');
                        $sidebar4 = ob_get_contents();
                        ob_end_clean();
                        $sidebar_corrected_ul4 = str_replace("<ul>", '<ul class="list-unstyled">', $sidebar4);
                        echo $sidebar_corrected_ul4;
                    }
                    ?>
                </div>
                
            </div>
            <?php /*
            <div class="row">
                <div class="text-center col">
                    <?php
                    if(is_active_sidebar('above-footer-column-4')) {
                        dynamic_sidebar('above-footer-column-4');
                    }
                    ?>
                    <a href="https://www.facebook.com/scott.geithman" target="_blank"><i class="fa fa-facebook-official fa-5x mr-2"></i></a>
                    <a href="https://twitter.com/ScottGeithman" target="_blank"><i class="fa fa-twitter-square fa-5x mr-2"></i></a>
                    <a href="https://www.linkedin.com/in/scottgeithman" target="_blank"><i class="fa fa-linkedin-square fa-5x mr-2"></i></a>
                    <a href="https://plus.google.com/102641868199427329349/about" target="_blank"><i class="fa fa-google-plus-square fa-5x"></i></a>
                </div>
            </div>
            */ ?>
        </div>
    </section>
    <?php /*
    <section class="bg-pink" style="padding: 40px;">
        <div class="container">
            section bg-pink
        </div>
    </section>
    
    <section class="bg-black" style="padding: 40px;">
        <div class="container">
            bg-black
        </div>
    </section>
    
    <section class="bg-neon-green" style="padding: 40px;">
        <div class="container">
            bg-neon-green
        </div>
    </section>
    
    <section class="bg-torquoise" style="padding: 40px;">
        <div class="container">
            bg-torquoise
        </div>
    </section>
    
    <section class="bg-white" style="padding: 40px;">
        <div class="container">
            bg- white
        </div>
    </section>
    
    <section class="bg-orange" style="padding: 40px;">
        <div class="container">
            bg-orange
        </div>
    </section>
    
    <section class="bg-swirl" style="padding: 40px;">
        <div class="container">
            bg-swirl
        </div>
    </section>
    */ ?>
    <footer class="footer bg-black text-white px-1 py-3">
        <?php /*
        <!-- footer navbar -->
        <nav class="navbar navbar-inverse navbar-footer">
            <div class="container">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-footer" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php /*<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/defero_logo_with_dots_white_cropped_left_206x40.png"/></a>* ?>
                <?php
                wp_nav_menu(array(
                    'theme_location'  => 'above_footer',
                    'menu'            => 'Footer Menu',
                    'container'       => 'div',
                    'container_class' => 'navbar-collapse collapse',
                    'container_id'    => 'navbar-footer',
                    'menu_class'      => 'nav navbar-nav',
                    'menu_id'         => 'navbar-footer-ul',
                    //'echo'            => true,
                    //'before'          => '',
                    //'after'           => '',
                    //'link_before'     => '',
                    //'link_after'      => '',
                    //'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 2,
                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                    'walker'          => new wp_bootstrap_navwalker(),
                    //'walker'          => new wp_bootstrap_navwalker_custom(),
                ));
                ?>
            </div>
        </nav>
		*/ ?>
        
        <div class="container text-center">
        	&copy; Copyright <?php echo date('Y'); ?> <a href="https://11gallery.com" target="_blank">11 Gallery, LLC</a>. All Rights Reserved.
            <?php // echo '<br />'.$_SERVER['SERVER_NAME']; ?>
        </div>
    </footer>

<?php wp_footer(); ?>

<script>
jQuery(document).ready(function($) {
	"use strict";
    console.log('load swipebox');
	$('.swipebox').swipebox();
    // $('.swipebox').swipebox({
    //     selector: '.swipebox',//needed for jQuery 3.x
    //     //useCSS : true, // false will force the use of jQuery for animations
    //     //useSVG : true, // false to force the use of png for buttons
    //     //initialIndexOnArray : 0, // which image index to init when a array is passed
    //     //hideCloseButtonOnMobile : false, // true will hide the close button on mobile devices
    //     hideBarsDelay : 0, // delay before hiding bars on desktop - 0 for always on
    //     //videoMaxWidth : 1140, // videos max width
    //     //beforeOpen: function() {}, // called before opening
    //     //afterOpen: null, // called after opening
    //     //afterClose: function() {} // called after closing
    //     //loopAtEnd: false, // true will return to the first image after the last image is reached
    //     //autoplayVideos: false // true will autoplay Youtube and Vimeo videos
    // });
    function parallax() {
        //start parallax position even on page refresh
        var yPos = -(jQuery(window).scrollTop() / 2);
        var coords = '50% '+ yPos + 'px';
        jQuery('.parallax').css({
            backgroundPosition: coords
        });
        //do parallax positioning on scroll
        jQuery(window).scroll(function() {
            yPos = -(jQuery(window).scrollTop() / 2);
            var coords = '50% '+ yPos + 'px';
            jQuery('.parallax').css({
                backgroundPosition: coords
            });
        }); 
    }
    parallax();
});
// hljs.configure({
//   tabReplace: '    ', // 4 spaces
//   //classPrefix: ''     // don't append class prefix
//                       // … other options aren't changed
// })
// hljs.initHighlighting();
</script>
<?php if($_SERVER['SERVER_NAME'] == 'trx400ex.com') { ?>
<!-- Default Statcounter code for Trx400ex.com https://trx400ex.com/ -->
<script type="text/javascript">
var sc_project=12245608; 
var sc_invisible=1; 
var sc_security="96943924"; 
</script>
<script type="text/javascript" src="https://www.statcounter.com/counter/counter.js" async></script>
<noscript>
    <div class="statcounter">
        <a title="Web Analytics" href="https://statcounter.com/" target="_blank">
            <img class="statcounter" src="https://c.statcounter.com/12245608/0/96943924/1/" alt="Web Analytics">
        </a>
    </div>
</noscript>
<!-- End of Statcounter Code -->
<?php } else { ?>
<!-- Default Statcounter code for <?php echo $_SERVER['SERVER_NAME']; ?> is not available -->
<?php } ?>
</body>
</html>
