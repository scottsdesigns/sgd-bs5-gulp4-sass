<?php
function get_blog_items($atts) {
    
	extract(shortcode_atts(array(
        'cat'            => 5,
        'posts_per_page' => -1,
    ), $atts ));
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$post_query = new WP_Query(array(
        'cat'            => $cat,
        'posts_per_page' => $posts_per_page,
        'post_type'      => 'post',
        'post_status'    => 'publish',
		'paged'          => $paged,
        //'offset'         => offset(1, $page),
        //'paged'          => $page,
        //'orderby'        => 'date',
        //'order'          => 'DESC'
    ));
    //$output = $post_query;
	
    $output = '';
    if($post_query->have_posts()) {
        $post_count = 0;
        $output .= '<div class="row">';
        while($post_query->have_posts()) {
            $post_query->the_post();
			//$categories = get_the_terms(get_the_id(), 'category');
			$post_image = get_the_post_thumbnail();
			//$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID(), 'large' ));
            $output .= '<article class="card mb-4">
                    <a href="'.get_the_permalink().'" title="'.get_the_title().'">
                        <img src="'.get_the_post_thumbnail_url(get_the_ID(), 'large').'" class="card-img-top" title="'.get_the_title().'" alt="'.get_the_title().'"/>
                    </a>
                    <div class="card-body">
                        <div class="post-title mb-3">
                            <a href="'.get_the_permalink().'">
                                <h3 class="media-heading">'.get_the_title().'</h3>
                            </a>
                        </div>
                        <div class="post-meta" style="margin-bottom: 20px;">
                            <i class="fa fa-calendar fa-fw"></i> <span class="post-date">'.get_the_date().'</span> | <i class="fa fa-user-md"></i> <span class="post-author">'.get_the_author().'</span> | <i class="fa fa-cubes fa-fw"></i> <span class="post-category">'.get_the_category_list(', ','').'</span>
                        </div>
                        <div class="post-excerpt" style="margin-bottom: 20px;">
                            '.get_the_excerpt().'
                        </div>
                        <a href="'.get_the_permalink().'" class="btn btn-primary">Read More</a>
                    </div>
            </article>';
			// <a href="'.get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')).'">'.get_the_author().'</a>
            $post_count++;
        }
        $output .= '</div>';
		
		$big = 999999999; // need an unlikely integer
		$translated = __( 'Page', 'sgd' ); // Supply translatable string
		
		$pages = paginate_links(array(
				'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged')),
				'total' => $post_query->max_num_pages,
				'prev_next' => false,
				'type'  => 'array',
				'prev_next'   => TRUE,
				'prev_text'    => __('«'),
				'next_text'    => __('»'),
				'before_page_number' => '<span class="sr-only sr-only-focusable">'.$translated.'</span>'
			));
		if(is_array($pages)) {
			//$paged = (get_query_var('paged') == 0) ? 1 : get_query_var('paged');
			$output .= '<ul class="pagination">';
			foreach ($pages as $page) {
				$output .= '<li'.(strpos($page, 'current') !== false ? ' class="page-item active"' : ' class="page-item"').'>'.$page.'</li>';
			}
		   $output .= '</ul>';
		}
		
        wp_reset_query();
        wp_reset_postdata();
    }
    else {
        $output .= '<div class="col-xs-12">
            <h3>Nothing Found</h3>
        </div>';
    }
    return $output;
}
add_shortcode('blog', 'get_blog_items');