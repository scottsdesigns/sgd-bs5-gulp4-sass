<?php
function get_custom_query($atts) {

   // EXAMPLE USAGE:
   // [loop the_query="showposts=100&post_type=page&post_parent=453"]
   
   // Defaults
   extract(shortcode_atts(array(
      "the_query" => ''
   ), $atts));

   // de-funkify query
   $the_query = preg_replace('~&#x0*([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $the_query);
   $the_query = preg_replace('~&#0*([0-9]+);~e', 'chr(\\1)', $the_query);

   // query is made               
   query_posts($the_query);
   
   // Reset and setup variables
   $output = '';
   $temp_title = '';
   $temp_link = '';
   $products_arr = array();
   
   // the loop
   if (have_posts()) : while (have_posts()) : the_post();
   
      $temp_title = get_the_title($post->ID);
      $temp_link = get_permalink($post->ID);
      if($temp_image = get_field('product_image', $post->ID)) {
          $product_image = $temp_image['sizes']['medium'];
      }
      else {
          $product_image = '';
      }
      
      // output all findings - CUSTOMIZE TO YOUR LIKING
      //$output .= '<li><a href="'.$temp_link.'">'.$temp_title.'</a></li>';
      $output .= '<div class="col-xs-6 col-centered col-fixed">
                <div class="item">
                    <div class="content">
                        <a href="">
                            <img src="'.$product_image.'" class="p-img" title="" alt=""/>
                            <div class="product-title">
                                '.$temp_title.'
                            </div>
                            <div class="star-rating">
                                <i class="fa fa-star fa-fw"></i>
                                <i class="fa fa-star fa-fw"></i>
                                <i class="fa fa-star fa-fw"></i>
                                <i class="fa fa-star fa-fw"></i>
                                <i class="fa fa-star-o fa-fw"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>';
          
   endwhile; else:
   
      $output .= "nothing found.";
      
   endif;
   
   wp_reset_query();
   return $output;
   
}
add_shortcode('loop', 'get_custom_query');