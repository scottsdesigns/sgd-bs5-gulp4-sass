<?php
function get_posts_by_category($atts) {
    
	extract(shortcode_atts(array(
        'cat'            => array(28,29),//1,
        'posts_per_page' => -1,
    ), $atts ));
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$post_query = new WP_Query(array(
        'cat'            => $cat,
        'posts_per_page' => $posts_per_page,
        'post_type'      => 'post',
        'post_status'    => 'publish',
		'paged'          => $paged,
        //'offset'         => offset(1, $page),
        //'paged'          => $page,
        //'orderby'        => 'date',
        //'order'          => 'DESC'
    ));
    //$output = $post_query;
	
    $output = '';
    if($post_query->have_posts()) {
        $post_count = 0;
        $output .= '<div class="row">';
        while($post_query->have_posts()) {
            $post_query->the_post();
            $output .= '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 mt-3 mb-3">
                <div class="card">
                    <a href="'.get_the_permalink().'" title="'.get_the_title().'">
                        <img src="'.get_the_post_thumbnail_url(get_the_ID(), 'large').'" class="card-img-top" title="'.get_the_title().'" alt="'.get_the_title().'"/>
                    </a>
                    <a href="'.get_the_permalink().'">
                        <div class="card-body text-center p-1">
                            <h5 class="text-center px-2 my-3">'.get_the_title().'</h5>
                        </div>
                    </a>
                    <div class="d-grid gap-2 col-8 mx-auto pb-4">
                        <a href="'.get_the_permalink().'" class="btn btn-primary col-12">View More</a>
                    </div>
                    <div class="card-footer text-muted">
                        '.implode(', ', wp_get_post_categories(get_the_ID(),array('fields'=>'names'))).'
                    </div>
                </div>
            </div>';
            $post_count++;
        }
        $output .= '</div>';
		
		$big = 999999999; // need an unlikely integer
		$translated = __('Page', 'sgd'); // Supply translatable string
		
		$pages = paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged')),
            'total' => $post_query->max_num_pages,
            'prev_next' => false,
            'type'  => 'array',
            'prev_next'   => TRUE,
            'prev_text'    => __('«'),
            'next_text'    => __('»'),
            'before_page_number' => '<span class="sr-only sr-only-focusable">'.$translated.'</span>'
        ));
        if(is_array($pages)) {
            $output .= '<ul class="pagination">';
            foreach ($pages as $page) {
                $output .= '<li'.(strpos($page, 'current') !== false ? ' class="page-item active"' : ' class="page-item"').'>'.$page.'</li>';
            }
            str_replace('page-numbers','page-link',$output);
            $output .= '</ul>';
        }
		
        wp_reset_query();
        wp_reset_postdata();
    }
    else {
        $output .= '<div class="col-xs-12">
            <h3>Nothing Found</h3>
        </div>';
    }
    return $output;
}
add_shortcode('posts_by_category', 'get_posts_by_category');