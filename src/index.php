
<?php
$bs_version = '5.1.3';
$bs_icons_version = '5.1.3';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bootstrap <?php echo $bs_version; ?> Tester</title>
    <link rel="stylesheet" href="assets/fonts/bootstrap-icons.css?ver=<?php echo $bs_icons_version; ?>">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css?ver=<?php echo $bs_version; ?>" type="text/css" media="all" id="sgd-css-bootstrap-css" />
    <?php /*<link rel="stylesheet"  href="assets/css/app.min.css?ver=1.0.0" type="text/css" media="all" id="sgd-css-app-css" />*/ ?>
</head>
<body>

Write Your Content Here <i class="bi bi-arrow-left-square-fill"></i>   

<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js?ver=<?php echo $bs_version; ?>" id="sgd-js-bootstrap-js"></script>
<?php /*<script type="text/javascript" src="assets/js/app.min.js?ver=1.0.0" id="sgd-js-app-js"></script>*/ ?>
</body>
</html>