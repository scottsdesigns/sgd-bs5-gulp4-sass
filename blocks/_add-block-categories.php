<?php
// function sgd_block_categories($categories) {
//     return array_merge(
//         $categories,
//         [
//             [
//                 'slug'  => 'sgd-blocks',
//                 'title' => __( 'SGD Blocks', 'sgd-bs5-theme' ),
//             ],
//         ]
//     );
// }
// add_action('block_categories', 'sgd_block_categories', 10, 2);

function sgd_block_categories($categories) {
    $category_slugs = wp_list_pluck( $categories, 'slug' );
    return in_array('sgd-blocks', $category_slugs, true ) ? $categories : array_merge(
        array(
            array(
                'slug'  => 'sgd-blocks',
                'title' => __( 'SGD Blocks', 'sgd-blocks' ),
                'icon'  => null,
            ),
        ),
        $categories
    );
}
add_filter('block_categories', 'sgd_block_categories');