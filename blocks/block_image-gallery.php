<?php
add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

    // Check function exists.
    if(function_exists('acf_register_block_type')) {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'image-gallery',
            'title'             => __('Image Gallery'),
            'description'       => __('A custom image gallery block.'),
            'render_template'   => 'template-parts/blocks/image-gallery/image-gallery.php',
            'category'          => 'sgd-blocks',
            'icon'              => 'format-gallery',
            'keywords'          => array('image', 'gallery'),
            'enqueue_assets' => function() {
                if(is_admin()) {
                    wp_enqueue_style('sgd-css-bootstrap5', get_template_directory_uri().'/assets/css/bootstrap-custom.min.css', array(), '5.1.3');
                    // wp_enqueue_script( 'block-testimonial', get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.js', array('jquery'), '', true );
                }
            }
        ));
    }
}