/*!
 * gulp
 * $ npm install sass gulp-sass gulp-autoprefixer gulp-cssnano jshint gulp-jshint gulp-concat gulp-uglify gulp-rename del --save-dev
 */

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const concat = require('gulp-concat');

// ----- CSS -----
// -- app sass compiled
function appCss() {
    return gulp.src('src/sass/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 3 version'))
        .pipe(gulp.dest('assets/css'))
        .pipe(rename({suffix:'.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css'));
};
// -- bootstrap sass compiled with our new bootstrap settings
function bsCssCustom() {
    return gulp.src('src/sass/bootstrap-custom.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 3 version'))
        .pipe(gulp.dest('assets/css'))
        .pipe(rename({suffix:'.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css'));
};
// -- bootstrap sass compiled straight from node_modules folder
function bsCss() {
    return gulp.src('./node_modules/bootstrap/scss/bootstrap.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 3 version'))
        .pipe(gulp.dest('assets/css'))
        .pipe(rename({suffix:'.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/css'));
};
function bsJs() {
    return gulp.src('./node_modules/bootstrap/dist/js/**/*')
        .pipe(gulp.dest('assets/js'));
        //.pipe(notify({message:'Vendor JS task complete','onLast': true,'sound': false}));
};
function bsIcons() {
    return gulp.src('./node_modules/bootstrap-icons/font/**/*')
        .pipe(gulp.dest('assets/fonts'));
        //.pipe(notify({message:'Vendor JS task complete','onLast': true,'sound': false}));
};
// ----- JS -----
function appJs() {
    return gulp.src('src/js/app/**/*')
        .pipe(jshint(/*'.jshintrc'*/))
        .pipe(jshint.reporter('default'))
        .pipe(concat('app.js'))
        .pipe(gulp.dest('assets/js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'));
        //.pipe(notify({message:'App JS task complete','onLast': true,'sound': false}));
};
function pluginsJs() {
    return gulp.src('src/js/plugins/**/*')
        .pipe(gulp.dest('assets/js/plugins'));
        //.pipe(notify({message:'Plugins JS task complete','onLast': true,'sound': false}));
};
function vendorJs() {
    return gulp.src('src/js/vendor/**/*')
        .pipe(gulp.dest('assets/js/vendor'));
        //.pipe(notify({message:'Vendor JS task complete','onLast': true,'sound': false}));
};
// ----- src root files only -----
function srcFiles() {
    return gulp.src('src/**',{nodir:true})
        .pipe(gulp.dest('./'));
}
// ----- Images -----
function images() {
    return gulp.src('src/img/**/*')
        //take care of this now with tinypng.com (way better results)
        //.pipe(cache(imagemin({optimizationLevel:3,progressive:true,interlaced:true})))
        .pipe(gulp.dest('assets/img'));
        //.pipe(notify({message:'Images task complete','onLast': true,'sound': false}));
};
// ----- Fonts -----
function fonts() {
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('assets/fonts'));
        //.pipe(notify({message:'Fonts task complete','onLast': true,'sound': false}));
};
// ----- Plugins -----
function plugins() {
    return gulp.src('src/plugins/**/*')
        .pipe(gulp.dest('assets/plugins'));
        //.pipe(notify({message:'Fonts task complete','onLast': true,'sound': false}));
};

// -------------------- Tasks --------------------

// Clean
//gulp.task('clean', function() {
//    return del(['dist/css', 'dist/scripts', 'dist/images']);
//});

// Default task
//gulp.task('default', ['clean'], function() {
//    gulp.start('styles', 'scripts', 'images');
//});

// Watch
// gulp.task('build', ['app-css','bs-css','app-js','plugins-js','vendor-js','fonts','images'], function() {
    
// });

//gulp.task('build-bs', gulp.series('bs-css'));
// ----- gulp v3 -----
// gulp.task('build', ['app-css','bs-css','app-js','plugins-js','vendor-js','fonts','images'], function() {
    
// });
// ----- gulp v4 -----
// -- series() = tasks based on the order provided
// -- parallel() tasks in async order (indiscriminate of the listed order)
gulp.task('build', gulp.series(bsCss,bsJs,bsIcons,images,fonts,vendorJs,srcFiles), function() {
    
});
gulp.task('buildbs', gulp.series(bsCss, bsJs, bsIcons));
gulp.task('buildbscustom', gulp.series(bsCssCustom, bsJs, bsIcons));
gulp.task('buildbscustomcss', gulp.series(bsCssCustom));
gulp.task('buildthemeassets', gulp.series(appCss, bsCss, bsCssCustom, bsJs, bsIcons, appJs, vendorJs, pluginsJs, fonts, images, plugins));

// Watch
// gulp.task('watch', ['app-css','bs-css','app-js','plugins-js','vendor-js','fonts','images'], function() {
//     // Watch app .scss files
//     gulp.watch('src/scss/app/**/*.scss', ['app-css']);
//     // Watch bootstrap .scss files
//     gulp.watch('src/scss/vendor/bootstrap_v4.0.0/**/*.scss', ['bs-css']);
//     // Watch js app files
//     gulp.watch('src/js/app/**/*', ['app-js']);
//     // Watch js plugins files
//     gulp.watch('src/js/plugins/**/*', ['plugins-js']);
//     // Watch js vendor files
//     gulp.watch('src/js/vendor/**/*', ['vendor-js']);
//     // Watch font files
//     gulp.watch('src/fonts/**/*', ['fonts']);
//     // Watch image files
//     gulp.watch('src/img/**/*', ['images']);
// });