<?php
/**
 * The template for displaying the home page
 *
 */

get_header(); ?>

<style>
/******************* Timeline Demo - 11 *****************/
.main-timeline11{overflow:hidden;position:relative;padding:40px 0;}
.main-timeline11 a{text-decoration:none;}
.main-timeline11:before{content:"";width:7px;height:100%;background:#fff;position:absolute;top:0;left:50%;transform:translateX(-50%)}
.main-timeline11 .timeline{width:50%;padding-left:50px;float:right;position:relative}
.main-timeline11 .timeline:after,.main-timeline11 .timeline:before{position:absolute;content:"";top:50%;transform:translateY(-50%)}
.main-timeline11 .timeline:before{width:30px;height:30px;border-radius:50%;background:#eee;border:7px solid #fff;left:-15px}
.main-timeline11 .timeline:after{display:block;border-right:30px solid #cc0000;border-top:20px solid transparent;border-bottom:20px solid transparent;left:24px}
.main-timeline11 .timeline-content{display:block;padding:25px;border-radius:100px;background:#cc0000;position:relative}
.main-timeline11 .timeline:nth-child(odd) .timeline-content{box-shadow:-5px 5px 15px 0px rgba(0,0,0,0.5)}
.main-timeline11 .timeline:nth-child(even) .timeline-content{box-shadow:5px 5px 15px 0px rgba(0,0,0,0.5)}
.main-timeline11 .timeline-content:after,.main-timeline11 .timeline-content:before{content:"";display:block;width:100%;clear:both}
.main-timeline11 .timeline-content:hover{text-decoration:none}
.main-timeline11 .inner-content{width:70%;float:right;padding:15px 20px 15px 15px;background:#fff;border-radius:0 80px 80px 0;color:#cc0000}
.main-timeline11 .year{display:inline-block;font-size:50px;font-weight:600;color:#fff;position:absolute;top:50%;left:7%;transform:translateY(-50%)}
.main-timeline11 .title{font-size:24px;font-weight:600;text-transform:uppercase;margin:0 0 5px}
.main-timeline11 .description{margin:0 0 5px}
.main-timeline11 .timeline:nth-child(2n){padding:0 50px 0 0}
.main-timeline11 .timeline:nth-child(2n) .year,.main-timeline11 .timeline:nth-child(2n):before{left:auto;right:-15px}
.main-timeline11 .timeline:nth-child(2n) .year{right:7%}
.main-timeline11 .timeline:nth-child(2n):after{border-right:none;border-left:30px solid #cc0000;left:auto;right:24px;border-left-color:#ffcc00}
.main-timeline11 .timeline:nth-child(2n) .inner-content{float:none;border-radius:80px 0 0 80px;text-align:right}
.main-timeline11 .timeline:nth-child(2){margin-top:130px}
.main-timeline11 .timeline:nth-child(odd){margin:-130px 0 0}
.main-timeline11 .timeline:nth-child(even){margin-bottom:80px}
.main-timeline11 .timeline:first-child,.main-timeline11 .timeline:last-child:nth-child(even){margin:0}
.main-timeline11 .timeline:nth-child(2n) .timeline-content{background:#ffcc00}
.main-timeline11 .timeline:nth-child(2n),.main-timeline11 .timeline:nth-child(2n) .inner-content{color:#333}
.main-timeline11 .timeline:nth-child(3n) .timeline-content{background:#cca300}
.main-timeline11 .timeline:nth-child(3n),.main-timeline11 .timeline:nth-child(3n) .inner-content{color:#cca300}
.main-timeline11 .timeline:nth-child(3n):after{border-right-color:#cca300}
.main-timeline11 .timeline:nth-child(4n) .timeline-content{background:#333333}
.main-timeline11 .timeline:nth-child(4n),.main-timeline11 .timeline:nth-child(4n) .inner-content{color:#333333}
.main-timeline11 .timeline:nth-child(4n):after{border-left-color:#333333}
.main-timeline11 .timeline:nth-child(5n) .timeline-content{background:#999999}
.main-timeline11 .timeline:nth-child(5n),.main-timeline11 .timeline:nth-child(5n) .inner-content{color:#999999}
.main-timeline11 .timeline:nth-child(5n):after{border-right-color:#999999}
.main-timeline11 .timeline:nth-child(6n) .timeline-content{background:#990000}
.main-timeline11 .timeline:nth-child(6n),.main-timeline11 .timeline:nth-child(6n) .inner-content{color:#990000}
.main-timeline11 .timeline:nth-child(6n):after{border-left-color:#990000}
@media only screen and (max-width:1200px){.main-timeline11 .inner-content{width:80%}
.main-timeline11 .year{font-size:45px;left:10px;transform:translateY(-50%) rotate(-90deg)}
.main-timeline11 .timeline:nth-child(2n) .year{right:10px}
}
/* @media only screen and (max-width:990px){.main-timeline11 .year{font-size:40px;left:0}
.main-timeline11 .timeline:nth-child(2n) .year{right:0}
} */
@media only screen and (max-width:990px){.main-timeline11 .timeline:before,.main-timeline11:before{left:10px;transform:translateX(0)}
.main-timeline11 .timeline:nth-child(2n):after{border-left:none;border-right:30px solid #cc0000;right:auto;left:24px;border-right-color:#ffcc00}
.main-timeline11 .timeline,.main-timeline11 .timeline:nth-child(even),.main-timeline11 .timeline:nth-child(odd){width:100%;float:none;margin:0 0 30px}
.main-timeline11 .timeline:last-child{margin-bottom:0}
.main-timeline11 .timeline:nth-child(2n){padding:0 0 0 50px}
.main-timeline11 .timeline:before,.main-timeline11 .timeline:nth-child(2n):before{left:-2px}
.main-timeline11 .inner-content{width:85%}
.main-timeline11 .timeline:nth-child(2n) .inner-content{float:right;border-radius:0 80px 80px 0;text-align:left}
.main-timeline11 .timeline:nth-child(2n) .year{right:auto;left:0}
.main-timeline11 .timeline:nth-child(3n):after{border-left-color:#cca300}
.main-timeline11 .timeline:nth-child(4n):after{border-right-color:#333333}
.main-timeline11 .timeline:nth-child(5n):after{border-left-color:#999999}
.main-timeline11 .timeline:nth-child(6n):after{border-right-color:#990000}
}
@media only screen and (max-width:479px){.main-timeline11 .timeline-content{padding:15px}
.main-timeline11 .inner-content{width:80%}
.main-timeline11 .year{font-size:30px}
}
</style>
    <?php /*
    <section style="background-color: #eee;padding: 40px 0;">
        <div class="container">
            
            <div class="row text-center">
                
                <div class="col-sm-6 col-lg-3">
                    <i class="fa fa-briefcase feature-icon" style="font-size: 45px;"></i>
                    <h3 class="spaced-out-header h4 uppercase font-weight-bold">TRX400EX Resources</h3>
                    <p>Tips, tricks, and genral information about building and ATV or dirtbike. Check out news articles and insights about ATV building from some of the industries top mechanics and builders.</p>
                    <hr class="vertical-spacer d-block d-sm-none">
                </div>
                
                <div class="col-sm-6 col-lg-3">
                    <i class="icon-hammer-wrench feature-icon" style="font-size: 45px;"></i>
                    <h3 class="spaced-out-header h4 uppercase font-weight-bold">TRX400EX Tools</h3>
                    <p>Here you will find information on all the tools needed to build your next ATV or dirtbike. You will get in depth reviews as well as the ability to purchase some of the leading tools of the trade.</p>
                    <hr class="vertical-spacer d-block d-sm-none">
                </div>
                
                <div class="col-sm-6 col-lg-3">
                    <i class="icon-design feature-icon" style="font-size: 45px;"></i>
                    <h3 class="spaced-out-header h4 uppercase font-weight-bold">TRX400EX  Builds</h3>
                    <p>Check out all of the builds by 11 Gallery ATV as well as featured builds from some of the top builders accross the country and accross the internet. See nver before seen photos and videos of top builds.</p>
                    <hr class="vertical-spacer d-block d-sm-none">
                </div>
                
                <div class="col-sm-6 col-lg-3">
                    <i class="fas fa-newspaper feature-icon" style="font-size: 45px;"></i>
                    <h3 class="spaced-out-header h4 uppercase font-weight-bold">TRX400EX News</h3>
                    <p>Keep up with all of the ATV and dirtbike industry news and releases. See what the future has to offer in the realm of sport quads, utiliity quads, dirtbikes, and trikes. Be the first to know breaking news.</p>
                </div>
            
            </div>
        </div>
    </section>
    */ ?>
    <section id="timeline" class="section" style="background-color: #eee;padding: 0;">
        <div class="container">
            <?php //<h4>Timeline Style : Demo-11</h4> ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-timeline11">
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">1999</span>
                                <div class="inner-content">
                                    <h3 class="title">Fourtrax TRX400EX</h3>
                                    <p class="description">
                                        Honda introduces the 1999 TRX400ex Fourtrax in 1998! The engine was basically derived from the XR400 motocross, enduro, trail, street, dirt bike.<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/1999-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        The price was a whopping $5500! In 2000 the carburetor boot is upgraded to prevent cracking. The seat says "Fourtrax" in 1999 and 2000.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2001</span>
                                <div class="inner-content">
                                    <h3 class="title">Sportrax TRX400EX</h3>
                                    <p class="description">
                                        In 2001 the "Fourtrax" moniker was dropped, and "Sportrax" was introduced, and put in place of, on the seat cover.<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/2001-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        The yellow color was introduced alond with a new vent system on the carburetor. The battery also gets a cover and the rear fender gets a minor reshape.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2005</span>
                                <div class="inner-content">
                                    <h3 class="title">TRX400EX</h3>
                                    <p class="description">
                                        The 2005 Honda 400ex gets a full makeover and reverse is added to the transmission. The transmission and sprockets are also a different gear ratio.<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/2005-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        This new 2nd generation 400ex is 18 pounds heavier. The 2005 comes in yellow or red.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2006</span>
                                <div class="inner-content">
                                    <h3 class="title">TRX400EX</h3>
                                    <p class="description">
                                        In 2006 the color black is introduced and yellow is dropped from the option sheet.<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/2006-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        Also in 2006, the small shift return spring gets fixed to prevent it from breaking so easily.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2008</span>
                                <div class="inner-content">
                                    <h3 class="title">TRX400X</h3>
                                    <p class="description">
                                        In 2008 the Honda 400ex drops the "e" and it becomes the 400x. It also receives all new plastics as well as a new headlight design.<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/2008-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        The frame is reiforced a bit and the seat is also redesigned.
                                    </p>
                                </div>
                            </a>
                        </div>
                        <div class="timeline">
                            <a href="#" class="timeline-content">
                                <span class="year">2014</span>
                                <div class="inner-content">
                                    <h3 class="title">TRX400X</h3>
                                    <p class="description">
                                        2014 is sadly the last year for the Honda 400ex (400x).<br/>
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/timeline1/2014-Honda-TRX400EX.jpg" class="img-fluid" /><br/>
                                        Nothing has really changed from the 2008 Honda TRX400x model.
                                    </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="latestPosts" class="section section-featured-products bg-white"><?php // bg-turquoise-lt ?>
        <div class="container">
            <h3 class="media-heading" style="margin-bottom: 20px;">Latest Posts</h3>
            <?php echo do_shortcode('[posts_by_category]'); ?>
        </div>
    </section>
    <?php /*
    <section class="section bg-maroon text-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-10">
                    <?php //<h4 class="media-heading">My name is Scott Geithman but poeple just call me "Scotty G The Web Master". Have a look around at my complete website designs, graphic designs and web and desktop application designs.</h4> ?>
                    <h4 class="media-heading">11 Gallery ATV - custom YouTube builds. <a href="/shop">Products We Use</a> - Check out some of the tools, equipment, resources, lifts, stands and more, that we use here in the shop.</h4>
                </div>
                <div class="col-sm-3 col-md-2">
                    <a href="/shop" class="btn btn-light btn-block" style="margin-top: 10px;"><i class="fas fa-store mr-1"></i> View Products</a>
                </div>
            </div>
        </div>
    </section>
    */ ?>
    <?php /*
    <section class="section section-featured-products">
        <div class="container">
            <h3 class="media-heading" style="margin-bottom: 20px;">Featured ATV Products</h3>
            <?php
            // $content = apply_filters('the_content', $post->post_content);
            // echo do_shortcode(trim(get_the_content()));
            echo do_shortcode('[products visibility="featured" columns="6" limit="6" orderby="date"]');
            ?>
        </div>
    </section>
    */ ?>

<?php /* // // ----- Owl Carousel Slider ----- ?>
<style>
.profile-thumb {
    width: 100px;
    height: 100px;
    position: relative;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: -80px;
    margin-bottom: 1rem;
}
</style>
	<section id="slider" class="section section-latest-builds" style="background-color: #eee;"><?php // bg-turquoise-lt ?>
        <div class="container">
            <h2 class="text-center"><b>11 Gallery ATV YouTube Video Comments</b></h2>
            <div class="slider">
                <div class="owl-carousel">
                    <div class="slider-card">
                        <div class="d-flex justify-content-center align-items-center mb-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Honda-400ex-Build---Part-11---Clutch-Oil-Pump-Timing-Chain-Gear-Shift-Linkage-Drive-Gear-Sprocket.jpg" class="owl-item-img" alt="" >
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Denver-Wynn.jpg" class="profile-thumb img-thumbnail rounded-circle">
                        <h5 class="mb-0 text-center"><b>Denver Wynn</b></h5>
                        <p class="text-center p-3">Great video buddy. Thanks for including the little details. A lot of I have never done this before. Much appreciated.</p>
                    </div>
                    <div class="slider-card">
                        <div class="d-flex justify-content-center align-items-center mb-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Honda-400ex-Build---Part-11---Clutch-Oil-Pump-Timing-Chain-Gear-Shift-Linkage-Drive-Gear-Sprocket.jpg" class="owl-item-img" alt="">
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/James-David.jpg" class="profile-thumb img-thumbnail rounded-circle">
                        <h5 class="mb-0 text-center"><b>James David</b></h5>
                        <p class="text-center p-3">Great build and it's more than a help it's like the cool shop Teacher every tech shop should have! Rock on bro</p>
                    </div>
                    <div class="slider-card">
                        <div class="d-flex justify-content-center align-items-center mb-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/slide-3.jpg" class="owl-item-img" alt="">
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Denver-Wynn.jpg" class="profile-thumb img-thumbnail rounded-circle">
                        <h5 class="mb-0 text-center"><b>PHP MySQL Tutorials</b></h5>
                        <p class="text-center p-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam temporibus quidem magni qui doloribus.</p>
                    </div>
                    <div class="slider-card">
                        <div class="d-flex justify-content-center align-items-center mb-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/slide-4.jpg" class="owl-item-img" alt="">
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Denver-Wynn.jpg" class="profile-thumb img-thumbnail rounded-circle">
                        <h5 class="mb-0 text-center"><b>Javascript Tutorials</b></h5>
                        <p class="text-center p-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam temporibus quidem magni qui doloribus quasi.</p>
                    </div>
                    <div class="slider-card">
                        <div class="d-flex justify-content-center align-items-center mb-4">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/slide-5.jpg" class="owl-item-img" alt="">
                        </div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/owl-carousel/Denver-Wynn.jpg" class="profile-thumb img-thumbnail rounded-circle">
                        <h5 class="mb-0 text-center"><b>Bootstrap Tutorials</b></h5>
                        <p class="text-center p-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam temporibus quidem magni qui doloribus.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>*/ ?>

	<?php while(have_posts()) : the_post(); ?>
    
	<?php endwhile; ?>
    <?php /*
    <section class="section text-white" style="background-color: #990000;">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-md-10">
                    <?php //<h4 class="media-heading">My name is Scott Geithman but poeple just call me "Scotty G The Web Master". Have a look around at my complete website designs, graphic designs and web and desktop application designs.</h4> ?>
                    <h4 class="media-heading">11 Gallery ATV - custom YouTube builds. <a href="https://www.youtube.com/11GalleryATV" target="_blank">YouTube.com/11GalleryATV</a> - Latest complete build: Honda 400ex from scratch.</h4>
                </div>
                <div class="col-sm-3 col-md-2">
                    <a href="https://www.youtube.com/11GalleryATV" target="_blank" class="btn btn-light btn-block" style="margin-top: 10px;"><i class="fab fa-youtube mr-1"></i> View Videos</a>
                </div>
            </div>
        </div>
    </section>
        
	<section class="section section-latest-builds" style="background-color: #d4af37;"><?php // bg-turquoise-lt ?>
        <div class="container">
            <h3 class="media-heading" style="margin-bottom: 20px;">Latest ATV YouTube Build Videos</h3>
            <?php
            // $content = apply_filters('the_content', $post->post_content);
            // echo do_shortcode(trim(get_the_content()));
            echo do_shortcode('[posts_by_category cat="25" posts_per_page="-1"]');
            ?>
        </div>
    </section>
	*/ ?>
	<?php
	if(is_active_sidebar('home_page_bottom_widget_area')) {
		dynamic_sidebar('home_page_bottom_widget_area');
	}
	?>

<?php get_footer(); ?>
