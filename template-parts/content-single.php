<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage SGD_Bootstrap_5_Gulp_4_Sass
 * @since SGD Bootstrap 5 Gulp 4 Sass 1.0.0
 */
?>
<?php
$post_type = get_post_type(get_the_ID());
if($post_type === 'wpdmpro') {
    $categories = get_the_term_list(get_the_ID(),'wpdmcategory', '', ', ');
    //$categories = strip_tags(get_the_term_list(get_the_ID(),'wpdmcategory', '', ', '));
}
else {
    $categories = get_the_category_list(', ','');
    //$categories = strip_tags(get_the_category_list(', ',''));
}
?>
    
    <section class="section section-post-single">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <article id="post-<?php the_ID(); ?>" <?php post_class('card'); ?>>
                        
                        <a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>" class="card-img-top" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>"/>
                        </a>
                    	
                        <div class="card-body">
                            
                            <div class="post-title" style="margin-bottom: 15px;">
                                <a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                    <h2 class="media-heading"><?php echo get_the_title(); ?></h2>
                                </a>
                            </div>
                            <div class="post-meta" style="margin-bottom: 20px;">
                                <i class="fa fa-calendar fa-fw"></i> <span class="post-date"><?php echo get_the_date(); ?></span> | <i class="fa fa-user-md"></i> <span class="post-author"><?php echo get_the_author(); ?></span> | <i class="fa fa-cubes fa-fw"></i> <span class="post-category"><?php echo $categories; ?></span>
                            </div>
                        
                            <div class="entry-content" style="margin-bottom: 20px;">
                                
<?php
// $attachment_id = 203;  // get_post_thumbnail_id(get_the_ID()); // (PDF on dev)
// $wp_get_attachment_metadata = wp_get_attachment_metadata($attachment_id);
// $thumbnail_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
// $thumbnail_title = get_the_title($attachment_id);
// $thumbnail_caption = wp_get_attachment_caption($attachment_id);
// $thumbnail_url = wp_get_attachment_url($attachment_id);
// //$thumbnail_sizes = get_post_thumbnail_id(get_the_ID());
// echo '<pre>';
// echo 'alt = '.$thumbnail_alt.'
// title = '.$thumbnail_title.'
// caption = '.$thumbnail_caption.'
// url = '.$thumbnail_url.'
// metadata = ';
// print_r($wp_get_attachment_metadata);
// echo '</pre>';
?>
<?php 
/*$images = get_field('post_image_gallery');
if( $images ): ?>
    <div class="row">
        <?php foreach( $images as $image ): ?>
        <!-- Gallery item -->
        <div class="col-md-6 col-lg-4 mb-4">
            <?php // echo '<pre>'; print_r($image); echo '</pre>'; ?>
            <a href="<?php echo esc_url($image['sizes']['1536x1536']); ?>"  class="swipebox text-decoration-none" rel="gallery-1">
                <div class="rounded shadow-sm" style="background-color: rgba(237,237,237,.75);">
                    <img src="<?php echo esc_url($image['sizes']['large']); ?>" title="<?php echo esc_attr($image['title']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-fluid card-img-top">
                    <div class="p-3">
                        <h5><?php echo esc_attr($image['title']); ?></h5>
                        <p class="small text-muted mb-0"><?php echo esc_html($image['caption']); ?></p>
                        <div class="d-flex align-items-center justify-content-between rounded-pill bg-light border px-3 py-2 mt-4">
                            <p class="small mb-0"><i class="fa fa-picture-o mr-2"></i><span class="font-weight-bold"><?php echo esc_html($image['subtype']); ?></span></p>
                            <div class="badge bg-primary px-3 rounded-pill font-weight-normal"><?php echo size_format($image['filesize'], 2); ?></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- End -->
        <?php endforeach; ?>
    </div>
<?php endif;*/ ?>
                            
                                <?php the_content(); ?>
                                
                                <?php
                                // if($markdown = get_field('markdown')) {
                                //     echo '<hr />';
                                //     include get_parent_theme_file_path('/custom/Parsedown.php');
                                //     include get_parent_theme_file_path('/custom/ParsedownExtra.php');
                                //     $parsedown = new ParsedownExtra();
                                //     $parsed = $parsedown->text($markdown/*htmlspecialchars_decode($item->content)*/);
                                //     echo $parsed;
                                // }
                                ?>
                                
                                <?php
                                wp_link_pages( array(
                                    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sgd-bs5-gulp4-sass' ) . '</span>',
                                    'after'       => '</div>',
                                    'link_before' => '<span>',
                                    'link_after'  => '</span>',
                                    'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'sgd-bs5-gulp4-sass' ) . ' </span>%',
                                    'separator'   => '<span class="screen-reader-text">, </span>',
                                ) );
                    
                                if('' !== get_the_author_meta('description')) {
                                    get_template_part('template-parts/biography');
                                }
                                ?>
                            </div>
                            
                            <?php
                            /*if(get_the_tag_list()) {
                                echo get_the_tag_list('<div class="post-tags"><i class="fa fa-tags fa-fw"></i> ',', ','</div>');
                            }*/
                            if(get_the_tag_list()) {
                                echo get_the_tag_list('<div class="post-tags"><i class="fa fa-tags fa-fw"></i> ','&nbsp;','</div>');
                            }
                            ?>
                            
                        </div>
                        
                    </article>
                    
                    <div class="">
                    <?php
					if(comments_open() || get_comments_number()) {
						comments_template();
					}
		            /*
					if(is_singular('attachment')) {
						// Parent post navigation.
						the_post_navigation( array(
							'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'sgd-bs5-gulp4-sass' ),
						));
					}
                    elseif (is_singular('post')) {
						// Previous/next post navigation.
						the_post_navigation( array(
							'next_text' => '<span class="meta-nav" aria-hidden="true"><strong>' . __( 'Next: ', 'sgd-bs5-gulp4-sass' ) . '</strong></span> ' .
								'<span class="screen-reader-text">' . __( 'Next post:', 'sgd-bs5-gulp4-sass' ) . '</span> ' .
								'<span class="post-title">%title</span>',
							'prev_text' => '<span class="meta-nav" aria-hidden="true"><strong>' . __( 'Prev: ', 'sgd-bs5-gulp4-sass' ) . '</strong></span> ' .
								'<span class="screen-reader-text">' . __( 'Previous post:', 'sgd-bs5-gulp4-sass' ) . '</span> ' .
								'<span class="post-title">%title</span>',
						) );
					}*/
                    ?>
                    <?php
					/*edit_post_link(
						sprintf(
							// translators: %s: Name of current post
							__( 'Edit This Post<span class="screen-reader-text"> "%s"</span>', 'sgd-bs5-gulp4-sass' ),
							get_the_title()
						),
						'<span class="edit-link">',
						'</span>'
					);*/
                    ?>
                    </div>
                </div>
                <aside class="col-md-4">
                    <?php get_sidebar(); ?>
                </aside>
            </div>
        </div>
    </section>
