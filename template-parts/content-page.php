<?php
/**
 * The template used for displaying page content
 *
 */
 
 $show_sidebar = false;//get_field('show_sidebar');
 $portfolio_page = false;//get_field('portfolio_page');
 $blog_page = true;//get_field('blog_page');
?>

<section id="post-<?php the_ID(); ?>" <?php post_class('section section-page'); ?>>
    <div class="container">
        <div class="row"> <?php echo $show_sidebar ? '<div class="col-md-8">' : '<div class="col-sm-12">'; ?>
            <?php the_content(); ?>
            <?php //echo $portfolio_page || $blog_page ? do_shortcode(trim(get_the_content())) : get_the_content(); ?>
            <?php //echo do_shortcode('[portfolio category_in="5" per_page="12"]'/*get_the_content()*/); ?>
            <?php 
            /*wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'sgd' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
                'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'sgd' ) . ' </span>%',
                'separator'   => '<span class="screen-reader-text">, </span>',
            ) );*/
            ?>
            <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    if(comments_open() || get_comments_number()) {
                        comments_template();
                    }
                    ?>
            <?php echo $show_sidebar ? '</div>' : ''; ?>
            <?php if($show_sidebar) { ?>
            <aside class="col-md-4">
                <?php get_sidebar(); ?>
            </aside>
            <?php } ?>
        </div>
    </div>
    <?php
            edit_post_link(
                sprintf(
                    /* translators: %s: Name of current post */
                    __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
                    get_the_title()
                ),
                '<section class="footer-admin"><div class="container"><span class="edit-link">',
                '</span></div></section><!-- .entry-footer -->'
            );
        ?>
</section>
