<?php

/**
 * Image Gallery Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// create id attribute allowing for custom "anchor" value.
$id = 'image_gallery-' . $block['id'];
if(!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// create class attribute allowing for custom "className" and "align" values.
$className = 'block-image-gallery';
if(!empty($block['className'])) {
    $className .= ' ' . $block['className'];
}
if(!empty($block['align'])) {
    $className .= ' align' . $block['align'];
}

// load the variables
$images = get_field('post_image_gallery');
if( $images ): ?>
    <div class="row">
        <?php foreach( $images as $image ): ?>
        <!-- Gallery item -->
        <div class="col-md-6 col-lg-4 mb-4">
            <?php // echo '<pre>'; print_r($image); echo '</pre>'; ?>
            <a href="<?php echo esc_url($image['sizes']['1536x1536']); ?>"  class="swipebox text-decoration-none" rel="gallery-1">
                <div class="rounded shadow-sm" style="background-color: rgba(237,237,237,.75);">
                    <img src="<?php echo esc_url($image['sizes']['large']); ?>" title="<?php echo esc_attr($image['title']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-fluid card-img-top">
                    <div class="p-3">
                        <h5><?php echo esc_attr($image['title']); ?></h5>
                        <p class="small text-muted mb-0"><?php echo esc_html($image['caption']); ?></p>
                        <div class="d-flex align-items-center justify-content-between rounded-pill bg-light border px-3 py-2 mt-4">
                            <p class="small mb-0"><i class="fa fa-picture-o mr-2"></i><span class="font-weight-bold"><?php echo esc_html($image['subtype']); ?></span></p>
                            <div class="badge bg-primary px-3 rounded-pill font-weight-normal"><?php echo size_format($image['filesize'], 2); ?></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!-- End -->
        <?php endforeach; ?>
    </div>
<?php endif; ?>