<?php
/*
requires ...
---- plugins ----
advanced-custom-fields-pro
intuitive-custom-post-order
---- fonts ----
font-awesome
*/

function return_title() {
    if(is_front_page()) {
        return 'Home';
    }
    elseif(is_home()) {
        //return 'WooCommerce';
        return single_post_title( '', false );
    }
    elseif(is_woocommerce() && is_product()) {
        return wp_title('');
    }
    elseif(is_woocommerce()) {
        return woocommerce_page_title();
    }
    elseif(is_archive()) {
        return get_the_archive_title();
    }
    elseif(is_category()) {
        return single_cat_title('', false);
    }
    elseif(is_search()) {
        return 'Search: '.$_GET['s'];
    }
    else {
        return wp_get_post_parent_id(get_the_ID()) ? get_the_title(wp_get_post_parent_id(get_the_ID())).' / '.get_the_title() : get_the_title();
    }
}
?>
<?php function echo_breadcrumbs_nav() { ?>
        <nav class="mb-0 pb-1">
            <ol class="breadcrumb mb-0">
                <?php if(is_front_page()) { ?>
                <li class="breadcrumb-item active"><a class="mb active" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
                <?php } elseif(is_home()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
                <li class="breadcrumb-item active"><?php echo single_post_title( '', false ); ?></li>
                <?php } elseif(is_woocommerce() && is_product()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/shop')); ?>">Products</a></li>
				<li class="breadcrumb-item active"><?php echo woocommerce_page_title(); ?></li>
                <?php } elseif(is_woocommerce()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
				<li class="breadcrumb-item active"><?php echo woocommerce_page_title(); ?></li>
                <?php } elseif(is_archive()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
				<li class="breadcrumb-item active"><?php echo get_the_archive_title(); ?></li>
				<?php } elseif(is_category()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
				<li class="breadcrumb-item active"><?php echo single_cat_title('', false); ?></li>
				<?php } elseif(is_search()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
				<li class="breadcrumb-item active"><?php echo 'Search: '.$_GET['s']; ?></li>
                <?php } elseif(is_single()) { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
                    <?php if(in_category('blog')) { ?>
                    <li class="breadcrumb-item active">Blog</li>
                    <?php } elseif(in_category('portfolio')) { ?>
                    <li class="breadcrumb-item active">Portfolio</li>
                    <?php } else { ?>
                    <li class="breadcrumb-item active">
                        <?php echo '<a href="'.esc_url(home_url('/blog-posts')).'">' ?>Posts</a>
                    </li>
                    <?php } ?>
                <?php } else { ?>
                <li class="breadcrumb-item"><a class="mb" href="<?php echo esc_url(home_url('/')); ?>"><i class="fa fa-home fa-fw"></i> Home</a></li>
                <?php echo wp_get_post_parent_id(get_the_ID()) ? '<li class="breadcrumb-item"><a class="mb" href="'.get_the_permalink(wp_get_post_parent_id(get_the_ID())).'">'.get_the_title(wp_get_post_parent_id(get_the_ID())).'</a></li>' : ''; ?>
                <li class="breadcrumb-item active"><?php echo get_the_title(); ?></li>
                <?php } ?>
            </ul>
        </nav>
<?php } ?>

<?php if(is_front_page()) { ?>

<style>
/* .parallax {
    transform: translate3d(0px, 0px, 0px);
    transition: 0s linear;
    transition-property: background-position;
    scroll-behavior: smooth;
} */
/* .carousel {
    height: 100vh;
    width: 100%;
    overflow:hidden;
}
.carousel .carousel-inner {
    height:100%;    
} */
.carousel {
    
}
.carousel-inner {
    text-align: center;
}
.carousel .carousel-inner .carousel-item {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}
.carousel .carousel-inner .carousel-item img {
    vertical-align: middle;
    max-height: 100%;
    max-width: 100%;
}
.carousel-caption {
    background-color: #fc0;
    padding-bottom: 1.25rem;
    width: 400px;
    margin-left: auto;
    margin-right: auto;
    border: 4px solid #fff;
    border-radius: 6px;
    bottom: 3.5rem;
    box-shadow: 5px 5px 5px rgba(0,0,0,.25);
}
</style>
    <?php /*<!-- Section Large Hero Image Profile Parallax
    ==================================================-->
    <section class="bkn parallax" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/hero-top-home_1600x1000_2_tinypng.jpg');background-position: 50% 0px;"><?php //0px ?>
        
        <div class="by">
            <div class="bko">
                <img class="rl bkp" src="<?php echo get_template_directory_uri(); ?>/assets/img/IMG_9118_110x110_tinypng.jpg" title="" alt="">
                <img class="rl bkp" src="<?php echo get_template_directory_uri(); ?>/assets/img/IMG_3835_110x110_tinypng.jpg" title="" alt="">
                <h4 class="bkr"><?php bloginfo('name'); ?></h4>
                <h1 class="bkq h6 font-weight-normal"><?php bloginfo('description'); ?></h1>
            </div>
        </div>
        
        <?php echo_breadcrumbs_nav(); ?>
        
    </section>*/ ?>

    <div id="carouselExampleDark" class="carousel carousel-dark slide carousel-fade bg-white" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/2008-2014.jpg" title="" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h3>2014 400x</h3>
                    <h5>(2008-2014) Gen 3</h5>
                    <p class="mb-0">2008, 2009, 2012, 2013, 2014<br />Honda TRX400X.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/2005-2007.jpg" title="" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h3>2006 400ex</h3>
                    <h5>(2005-2007) Gen 2</h5>
                    <p class="mb-0">2005, 2006, 2007<br />Honda TRX400EX.</p>
                </div>
            </div>
            <div class="carousel-item"><?php // data-bs-interval="10000" ?>
                <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/img/slider/1999-2004.jpg" title="" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <h3>2001 400ex</h3>
                    <h5>(1999-2004) Gen 1</h5>
                    <p class="mb-0">1999, 2000, 2001, 2002, 2003, 2004<br />Honda TRX400EX.</p>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
    
<?php } elseif(is_page('contact')) { ?>
    
    <!-- Section Hero Title with Nav
    ==================================================-->
    <section class="bkn bg-maroon-dk text-white" style="padding: 20px 0 40px 0;height: 0px;">
        <div class="container-fluid">
            <h1 class="mb-3"><?php echo return_title(); ?></h1>
        </div>
        <?php echo_breadcrumbs_nav(); ?>
    </section>
    
    <!-- Section Hero Map
    ==================================================-->
    <section class="section-hero section-hero-map" style="border-bottom: 1px solid #ddd;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.6891894291575!2d-112.0156391842562!3d33.50946275358377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872b0d103bfc5761%3A0x70e59b5b54909a74!2s3131+E+Camelback+Rd%2C+Phoenix%2C+AZ+85016!5e0!3m2!1sen!2sus!4v1519318009350" width="100%" height="250" frameborder="0" style="border: 0;display: block;" allowfullscreen></iframe>
    </section>
    
<?php } elseif(is_page('about')) { ?>
    
    <!-- Section Small Hero Image Title with Nav
    ==================================================-->
    <section class="bkn bg-dark section-hero-image-title pb-0 text-white" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/city-of-phoenix-2-dark-75.jpg);height: 200px;background-position: center center;">
        
        <div class="by">
            <h3><?php echo return_title(); ?></h3>
        </div>
        
        <?php echo_breadcrumbs_nav(); ?>
        
    </section>
    
<?php } else { ?>
    
    <!-- Section Hero Title with Nav
    ==================================================-->
    <section class="bg-secondary">
        <div class="container">
            <h1><?php echo return_title(); ?></h1>
            <?php echo_breadcrumbs_nav(); ?>
        </div>
    </section>
    <?php /*
    <!-- Section Large Hero Image Profile
    ==================================================-->
    <section class="bkn bg-dark" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/city-of-phoenix-dark-75-1528x675-2.jpg);">
        
        <div class="by">
            <div class="bko">
                <img class="rl bkp" src="<?php echo get_template_directory_uri(); ?>/assets/img/me_tie_110x110.jpg" title="" alt=""> <img class="rl bkp" src="<?php echo get_template_directory_uri(); ?>/assets/img/me_guitar_110x110.jpg" title="" alt="">
                <h4 class="bkr">Scotty G the Webmaster</h4>
                <p class="bkq">
                    My name is Scott Geithman but people just call me "Scotty G The Web Master".
                </p>
            </div>
        </div>
        
        <?php echo_breadcrumbs_nav(); ?>
        
    </section>
    
    <!-- Section Hero Navigation
    ==================================================-->
    <section class="bg-maroon-dk" style="position: relative;padding-top: 60px;border-bottom: 1px solid #333333;">
        <?php echo_breadcrumbs_nav(); ?>
    </section>
    
    <!-- Section Hero Title
    ==================================================-->
    <section class="bg-maroon-dk" style="background-color: #5c5a4a;border-bottom: 1px solid #24241d;color: #fff;padding: 20px;">
        <div class="container-fluid text-center">
            <h1><?php echo return_title(); ?></h1>
        </div>
    </section>
    
    <!-- Section Hero Navigation
    ==================================================-->
    <section class="bg-maroon-dk" style="position: relative;padding-top: 60px;border-bottom: 1px solid #333333;">
        <?php echo_breadcrumbs_nav(); ?>
    </section>
    
    <!-- Section Hero Title with Nav
    ==================================================-->
    <section class="bkn bg-maroon-dk" style="background-color: #5c5a4a;border-bottom: 1px solid #24241d;color: #fff;padding: 20px 0 40px 0;height: 0px;">
        <div class="container-fluid">
            <h1 class="mb-3"><?php echo return_title(); ?></h1>
        </div>
        <?php echo_breadcrumbs_nav(); ?>
    </section>
    
    <!-- Section Small Hero Image Title
    ==================================================-->
    <section class="bkn bg-dark section-hero-image-title pb-0 text-white" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/city-of-phoenix-2-dark-75.jpg);height: 200px;background-position: center center;">
        
        <div class="by">
            <h3><?php echo return_title(); ?></h3>
        </div>
        
    </section>
    
    <!-- Section Small Hero Image Title with Nav
    ==================================================-->
    <section class="bkn bg-dark section-hero-image-title pb-0 text-white" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/city-of-phoenix-2-dark-75.jpg);height: 200px;background-position: center center;">
        
        <div class="by">
            <h3><?php echo return_title(); ?></h3>
        </div>
        
        <?php echo_breadcrumbs_nav(); ?>
        
    </section>
    */ ?>
<?php } ?>