<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php if(get_post_type() == 'videos') { ?>
<div class="col-sm-6 col-md-6">
    <div class="thumbnail">
        <a class="swipebox" rel="video-<?php the_ID(); ?>" href="<?php echo get_field('video_url'); ?>">
        	<img src="<?php video_thumbnail(); ?>" class="responsive" />
        </a>
        <div class="caption">
            <h4 class="video-title"><a class="swipebox" rel="video-<?php the_ID(); ?>" href="<?php echo get_field('video_url'); ?>"><?php the_title(); ?></a></h4>
            <?php /*<p><?php //echo excerpt(25); ?><?php the_excerpt(); ?></p>*/ ?>
            <a href="<?php echo get_permalink(); ?>">Single Video Player <i class="fa fa-external-link"></i></a>
        </div>
    </div>
</div>
<?php } else { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->
    
    <div class="image">
    	<a class="swipebox" rel="video-<?php the_ID(); ?>" href="<?php echo get_field('video_url'); ?>">
        	<img src="<?php video_thumbnail(); ?>" style="max-width:100%" />
        </a>
    </div>

	<?php //twentysixteen_excerpt(); ?>

	<?php //twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<?php } ?>