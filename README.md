# Wordpress Theme Starter
> Current Version: v1.0.1
> Built with Bootstrap 5 (v5.1.3).
> Compiled with Gulp 4 (CLI v2.3.0, Local v4.0.2)

### What to do?
1. install node.js
2. install gulp global
3. install gulp local (in project folder)
4. install gulp dependencies
5. install bootstrap
6. install bootstrap dependencies (popper)

Yes, you SHOULD:

1. commit the `package-lock.json`.
2. use `npm ci` instead of `npm install` when building your applications both on your CI and your local development machine

The `npm ci` workflow requires the existence of a `package-lock.json`.

---

gulp 4 `node_modules` to install

```
npm install sass gulp-sass gulp-autoprefixer gulp-cssnano jshint gulp-jshint gulp-concat gulp-uglify gulp-rename del --save-dev
```

gulpfile.js 3 vs gulpfile.js 4 structure changes

```
// ----- gulp v3 -----
// var gulp = require('gulp');
// ----- gulp v4 -----
const gulp = require('gulp');


// ----- gulp v3 -----
// gulp.task('vendor-js', function() {
//     return gulp.src('src/js/vendor/**/*')
//         .pipe(gulp.dest('assets/js/vendor'));
//         //.pipe(notify({message:'Vendor JS task complete','onLast': true,'sound': false}));
// });
// ----- gulp v4 -----
function vendorjs() {
    return gulp.src('src/js/vendor/**/*')
        .pipe(gulp.dest('assets/js/vendor'));
        //.pipe(notify({message:'Vendor JS task complete','onLast': true,'sound': false}));
};


// ----- gulp v3 -----
// gulp.task('build', ['app-css','bs-css','app-js','plugins-js','vendor-js','fonts','images'], function() {
    
// });
// ----- gulp v4 -----
gulp.task('build', gulp.series(bscss,bsjs,bsicons,images,fonts,vendorjs,srcfiles), function() {
    
});
```

bootstrap 5 / popper.js (optional)

```
npm install bootstrap --save-dev
npm install @popperjs/core --save-dev
```

---

## Directory Overview
```
sgd-bs5-gulp4-sass/
│
├── 404/
│
├── admin/
│   ├── admin-bar.php
│   ├── custom-columns.php
│   ├── customizer.php
│   └── enqueue-admin-scripts.php
│
├── blocks/
│   ├── _add-block-categories.php
│   └── block_image-gallery.php
│
├── custom/
│   ├── Parsedown.php
│   ├── ParsedownExtra.php
│   └── login.php
│
├── includes/
│   ├── enqueue-scripts.php
│   ├── navigation.php
│   ├── theme.php
│   ├── widget-areas.php
│   └── wp-bootstrap5.1.3-navwalker.php
│
├── plugins/ (add or remove files as plugins are installed or removed)
│   ├── advanced-custom-fields.php
│   └── woocommerce.php
│
├── shortcodes/
│   ├── get_blog_items.php
│   ├── get_custom_query.php
│   ├── get_portfolio_items.php
│   └── get_posts_by_category.php
│
├── src/
│   ├── img/
│   │   ├── icons/
│   │   ├── slider/
│   │   └── timeline/
│   │
│   ├── plugins/
│   │   └── swipebox-v1.5.2/
│   │
│   ├── sass/
│   │   ├── _layout.scss
│   │   ├── app.scss
│   │   └── bootstrap-custom.scss
│   │
│   ├── bs-icons.html
│   └── index.php
│
├── template-parts/
│   ├── blocks/
│   │   └── image-gallery/
│   │       └── image-gallery.php
│   │
│   ├── biography.php
│   ├── content-none.php
│   ├── content-page.php
│   ├── content-search.php
│   ├── content-single.php
│   ├── content-videos.php
│   ├── content.php
│   ├── sections-hero.php
│   └── sections-nav.php
│
├── .gitignore
├── 404.php
├── bs-icons.html
├── comments.php
├── footer.php
├── functions.php
├── gulpfile.js
├── header.php
├── index.php
├── package-lock.json
├── package.json
├── page-home.php
├── page.php
├── screenshot.png
├── screenshot.psd
├── screenshot_uncompressed.png
├── searchform.php
├── sidebar.php
├── single.php
├── style.css
└── woocommerce.php (optional)
```

---

You need this when you're developing on a Mac. Ignore it on Windows.

```
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.2.13 
```

## Useful Resources

- [ScottsDesigns](http://scottsdesigns.com)