<?php
register_nav_menu('in_header','Header Menu');
register_nav_menu('above_footer','Footer Menu');

add_filter('nav_menu_css_class' , 'filter_nav_menu_css_class' , 10 , 2);
function filter_nav_menu_css_class($classes, $item){
     if(in_array('current-menu-item', $classes) ){
         $classes[] = 'active ';
     }
     return $classes;
}

add_filter('nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 3);
function filter_nav_menu_link_attributes($atts, $item, $args) {
	// The ID of the target menu item
	if($_SERVER['SERVER_NAME'] != '11galleryatv.wp') {
		$menu_target = 25; //production
	}
	else {
		$menu_target = 25; //local
	}
	
	// inspect $item
	if($item->ID == $menu_target) {
		$atts['data-toggle'] = 'collapse';
		$atts['data-target'] = '#search-bar';
	}
	return $atts;
}

// function add_search_form($items, $args) {
//     if( $args->theme_location == 'in_header' ) {
//         $items .= '<li class="menu-item">'
//             . '<form role="search" method="get" class="search-form" action="'.home_url( '/' ).'">'
//             . '<label>'
//             . '<span class="screen-reader-text">' . _x( 'Search for:', 'label' ) . '</span>'
//             . '<input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search …', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', 'label' ) . '" />'
//             . '</label>'
//             . '<input type="submit" class="search-submit" value="'. esc_attr_x('Search', 'submit button') .'" />'
//             . '</form>'
//             . '</li>';
//     }
//     return $items;
// }
// add_filter('wp_nav_menu_items', 'add_search_form', 10, 2);