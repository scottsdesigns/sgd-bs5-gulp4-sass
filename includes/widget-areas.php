<?php
function sgd_widgets_init() {
	register_sidebar(array(
		'name'          => __( 'Sidebar', 'sgd' ),
		'id'            => 'sidebar-1',
        'class'         => 'list-group',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'sgd' ),
        'class'         => 'list-group',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __( 'Home Page Bottom', 'sgd' ),
		'id'            => 'home_page_bottom_widget_area',
		'description'   => __( 'Appears at the bottom of the home page.', 'sgd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h1 class="page-header">',
		'after_title'   => '</h1>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Section 1', 'sgd' ),
		'id'            => 'above-footer-section-1',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		//'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="container">',
		//'after_widget'  => '</div></section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Section 2', 'sgd' ),
		'id'            => 'above-footer-section-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		//'before_widget' => '<section id="%1$s" class="widget %2$s"><div class="container">',
		//'after_widget'  => '</div></section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Column 1', 'sgd' ),
		'id'            => 'above-footer-column-1',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="mb-3">',
		'after_title'   => '</h4>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Column 2', 'sgd' ),
		'id'            => 'above-footer-column-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="mb-3">',
		'after_title'   => '</h4>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Column 3', 'sgd' ),
		'id'            => 'above-footer-column-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="mb-3">',
		'after_title'   => '</h4>',
	));

	register_sidebar(array(
		'name'          => __( 'Above Footer Column 4', 'sgd' ),
		'id'            => 'above-footer-column-4',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'sgd' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="mb-3">',
		'after_title'   => '</h4>',
	));
}
add_action( 'widgets_init', 'sgd_widgets_init' );

