<?php
add_theme_support('html5',array('search-form'));
add_theme_support('post-thumbnails');

// add_image_size('slider-image', 1600, 517, array('center', 'center'));
// add_image_size('slider-thumb', 200, 65, array('center', 'center'));

// add_filter('term_links-post_tag', 'add_tag_class');
// function add_tag_class($links) {
//     return str_replace('<a href="', '<a class="badge badge-light" href="', $links);
// }

// function custom_excerpt_length( $length ) {
// 	return 20;
// }
// add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// function excerpt($limit) {
// 	$excerpt = explode(' ', get_the_excerpt(), $limit);
// 	if (count($excerpt)>=$limit) {
// 		array_pop($excerpt);
// 		$excerpt = implode(" ",$excerpt).'...';
// 	}
// 	else {
// 		$excerpt = implode(" ",$excerpt);
// 	}	
// 	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
// 	return $excerpt;
// }

// function content($limit) {
// 	$content = explode(' ', get_the_content(), $limit);
// 	if (count($content)>=$limit) {
// 		array_pop($content);
// 		$content = implode(" ",$content).'...';
// 	}
// 	else {
// 		$content = implode(" ",$content);
// 	}	
// 	$content = preg_replace('/\[.+\]/','', $content);
// 	$content = apply_filters('the_content', $content); 
// 	$content = str_replace(']]>', ']]&gt;', $content);
// 	return $content;
// }