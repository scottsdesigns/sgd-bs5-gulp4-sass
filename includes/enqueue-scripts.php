<?php
function sgd_scripts() {
	// ----- FONTS -----
	// wp_enqueue_style('sgd-css-roboto', get_template_directory_uri().'/fonts/roboto.css', array(), '1.1.0');
	// wp_enqueue_style('sgd-css-roboto-condensed', get_template_directory_uri().'/fonts/roboto-condensed.css', array(), '1.1.0');
	// wp_enqueue_style('sgd-css-open-sans', get_template_directory_uri().'/fonts/open-sans.css', array(), '1.1.0');
	// wp_enqueue_style('sgd-css-bebas-neue', get_template_directory_uri().'/fonts/bebas-neue.css', array(), '1.1.0');
	wp_enqueue_style('sgd-css-font-awesome', get_template_directory_uri().'/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css', array(), '4.7.0');
	// wp_enqueue_style('sgd-css-font-awesome-5', get_template_directory_uri().'/assets/fonts/fontawesome-free-5.3.1-web/css/all.min.css', array(), '5.3.1');
	wp_enqueue_style('sgd-css-font-awesome-5', get_template_directory_uri().'/assets/fonts/fontawesome-free-5.15.3-web/css/all.edit.min.css', array(), '5.15.3');
	// wp_enqueue_style('sgd-css-font-icomoon', get_template_directory_uri().'/assets/fonts/icomoon/styles.css', array(), '1.1.0');
	//CSS
	// wp_enqueue_style('sgd-css-bootstrap5', get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '5.1.3');
	wp_enqueue_style('sgd-css-bootstrap5', get_template_directory_uri().'/assets/css/bootstrap-custom.min.css', array(), '5.1.3');
	wp_enqueue_style('sgd-css-swipebox', get_template_directory_uri().'/assets/plugins/swipebox-v1.5.2/css/swipebox.min.css', array(), '1.5.2');
	wp_enqueue_style('sgd-css-app', get_template_directory_uri().'/assets/css/app.min.css', array(), '1.0.0');
	// wp_enqueue_style('sgd-css-swipebox', get_template_directory_uri().'/assets/js/plugins/swipebox/css/swipebox.min.css', array(), '1.4.4');
	// wp_enqueue_style('sgd-css-highlightjs', get_template_directory_uri().'/assets/js/plugins/highlightjs/styles/dreamweaver-recogneyes.css', array(), '9.12.0');
	// wp_enqueue_style('sgd-css-app', get_template_directory_uri().'/assets/css/app.min.css', array(), '1.0.0');
	//JS
    // if(!is_admin()) {
    //     wp_deregister_script('jquery');
    //     wp_register_script('jquery', get_template_directory_uri().'/assets/js/vendor/jquery-3.3.1.min.js', false, '3.3.1');
    //     wp_enqueue_script('jquery');
    // }
	
	// ----- Owl Carousel Slider -----
	// wp_enqueue_style('plugin-owl-carousel-css', get_template_directory_uri().'/assets/plugins/owl-carousel/css/owl.carousel.min.css', array(), '2.3.4' );
	// wp_enqueue_style('plugin-owl-carousel-styles', get_template_directory_uri().'/assets/plugins/owl-carousel/css/styles.css', array(), '2.3.4' );
    
	wp_enqueue_script('sgd-js-bootstrap5', get_template_directory_uri().'/assets/js/bootstrap.bundle.min.js', array(), '5.1.3', true);
    wp_enqueue_script('sgd-js-swipebox', get_template_directory_uri().'/assets/plugins/swipebox-v1.5.2/js/jquery.swipebox.min.js', array('jquery'), '1.5.2', true);
	// wp_enqueue_script('sgd-js-popper', get_template_directory_uri().'/assets/js/plugins/popper.min.js', array('jquery'), '4.0.0', true);
    // wp_enqueue_script('sgd-js-bootstrap', get_template_directory_uri().'/assets/js/vendor/bootstrap_v4.3.1/bootstrap.bundle.min.js', array('jquery'), '4.3.1', true);
	// wp_enqueue_script('sgd-js-swipebox', get_template_directory_uri().'/assets/js/plugins/swipebox/js/jquery.swipebox.min.js', array('jquery'), '1.4.4', true);
    // wp_enqueue_script('sgd-js-highlightjs', get_template_directory_uri().'/assets/js/plugins/highlightjs/highlight.pack.js', array('jquery'), '9.12.0', true);
	// wp_enqueue_script('sgd-js-app', get_template_directory_uri().'/assets/js/app.min.js', array('jquery'), '1.0.0', true);
	
	// ----- Owl Carousel Slider -----
	// wp_enqueue_script('plugin-owl-carousel-js', get_template_directory_uri().'/assets/plugins/owl-carousel/js/owl.carousel.min.js', array('jquery'), '2.3.4', true);
	// wp_enqueue_script('plugin-owl-carousel-script', get_template_directory_uri().'/assets/plugins/owl-carousel/js/script.js', array('jquery'), '2.3.4', true);
}
add_action('wp_enqueue_scripts', 'sgd_scripts');