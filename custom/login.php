<?php
function sgd_login_logo() { ?>
    <style type="text/css">
        body.login {
			background-color: #333333 !important;/* #3d0c16 - dker maroon, #671425 - dk maroon, #a31e39 - maroon, #485c5a - dk turquoise */
		}
		.login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/11GalleryATV_all-horizontal_text-white-shadow-333333@0.5x.png) !important;
			background-size: 284px auto !important;
            padding-bottom: 0px !important;
			width: 284px !important;
			height: 54px !important;
        }
        .login #backtoblog {
			text-align: center !important;/*if remove_lost_password is disabled*/
		}
		.login #backtoblog a, .login #nav a, .login h1 a {
			text-decoration: none !important;
			color: #cccccc !important;
		}
		.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
			text-decoration: none !important;
			color: #999999 !important;
		}
    </style>
<?php }
add_action('login_enqueue_scripts', 'sgd_login_logo');

function sgd_login_logo_url_title() {
	return 'Back to Homepage'; 
} 
add_filter('login_headertitle', 'sgd_login_logo_url_title');

function custom_loginlogo_url($url) {
	return esc_url(home_url('/'));
}
add_filter('login_headerurl', 'custom_loginlogo_url');

//link is "/wp-login.php?action=lostpassword" in case you need it directly
function sgd_remove_lost_password ($text) {
	if ($text == 'Lost your password?'){
		$text = '';
	}
	return $text;
}
add_filter('gettext', 'sgd_remove_lost_password');